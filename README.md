# `rayt`: A Bidirectional Path Tracer in Rust

This repo contains an implementation of [bidirectional path tracing](https://en.wikipedia.org/wiki/Path_tracing#Bidirectional_path_tracing) with multiple importance sampling, written in Rust. It was written largely based on one of the original papers on the subject: Veach, Eric, and Leonidas J. Guibas. "Optimally combining sampling techniques for Monte Carlo rendering." Proceedings of the 22nd annual conference on Computer graphics and interactive techniques. 1995.

This is just a personal project of mine, and isn't designed to be a general-purpose 3D rendering code, but could be useful as a reference. It's black and white only.

Bidirectional path tracing is an unbiased Monte Carlo technique for rendering 3D scenes in computer graphics. It is widely used in professional rendering codes, e.g. in Pixar movies.
