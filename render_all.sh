HEIGHT_PIXELS=1000
NUM_SAMPLES=100

cargo run --bin rayt --release -- mirror_balls  --eye_trace_depth=100 --light_trace_depth=0 --height_pixels="$HEIGHT_PIXELS" --num_samples=1
cargo run --bin rayt --release -- mirror_balls_b --eye_trace_depth=4 --light_trace_depth=4 --height_pixels="$HEIGHT_PIXELS" --num_samples=$NUM_SAMPLES

cargo run --bin rayt --release -- breakfast --eye_trace_depth=4 --light_trace_depth=4 --height_pixels="$HEIGHT_PIXELS" --num_samples=500
cargo run --bin rayt --release -- breakfast_b --eye_trace_depth=4 --light_trace_depth=4 --height_pixels="$HEIGHT_PIXELS" --num_samples=500

cargo run --bin rayt --release -- moonlit --eye_trace_depth=2 --light_trace_depth=1 --height_pixels="$HEIGHT_PIXELS" --num_samples=$NUM_SAMPLES
cargo run --bin rayt --release -- moonlit_b --eye_trace_depth=2 --light_trace_depth=1 --height_pixels="$HEIGHT_PIXELS" --num_samples=$NUM_SAMPLES

cargo run --bin rayt --release -- wicker --eye_trace_depth=2 --light_trace_depth=6 --height_pixels="$HEIGHT_PIXELS" --num_samples=$NUM_SAMPLES
cargo run --bin rayt --release -- wicker_b --eye_trace_depth=2 --light_trace_depth=6 --height_pixels="$HEIGHT_PIXELS" --num_samples=$NUM_SAMPLES
