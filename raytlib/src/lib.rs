pub mod math;
pub mod render;
pub mod scene;
pub mod trace;

#[cfg(test)]
pub mod test_util;
pub type Float = f64;
pub use std::f64::consts::PI;

pub use math::{
    translation, DeterministicRandomEngine, EmitProfile, Isometry, OrientedNormal, Point,
    RandomSample, Ray, StatsStream, UnitVector, Vector, NORMAL_RNG_DIST,
};
pub use render::*;
pub use scene::*;
pub use trace::{DeterministicRayTracer, RayTracer};
