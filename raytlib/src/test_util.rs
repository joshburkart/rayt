use crate::math;
use crate::Float;
use approx;

pub fn make_ray(point: [Float; 3], dir: [Float; 3]) -> math::Ray {
    math::Ray::new(
        math::Point::from_slice(&point),
        math::UnitVector::new_normalize(math::Vector::from_column_slice(&dir)),
    )
}

pub fn assert_pointers_eq<T, U: std::borrow::Borrow<T>, V: std::borrow::Borrow<T>>(a: U, b: V) {
    assert_eq!(a.borrow() as *const T, b.borrow() as *const T);
}

pub trait AssertApproxEq {
    fn assert_approx_eq(&self, other: &Self, epsilon: Float);
}

impl AssertApproxEq for math::UnitVector {
    fn assert_approx_eq(&self, other: &Self, epsilon: Float) {
        for axis in 0..2 {
            approx::assert_relative_eq!(self[axis], other[axis], epsilon = epsilon);
        }
    }
}

impl AssertApproxEq for math::Vector {
    fn assert_approx_eq(&self, other: &Self, epsilon: Float) {
        for axis in 0..2 {
            approx::assert_relative_eq!(self[axis], other[axis], epsilon = epsilon);
        }
    }
}
