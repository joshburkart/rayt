use crate::math;
use crate::scene as sc;
use crate::Float;

const EPSILON: Float = 1e-7;

pub type RayTracer = RayTracerImpl<math::RandomSampleEngine>;
pub type DeterministicRayTracer = RayTracerImpl<math::DeterministicRandomEngine>;

#[derive(Clone, Debug)]
pub struct RayTracerImpl<R: math::RandomSample> {
    light_trace_depth: Option<usize>,
    eye_trace_depth: Option<usize>,

    min_num_samples: usize,
    max_num_samples: usize,
    converge_abs_tol: Float,

    rng: R,
}

impl<R: math::RandomSample> Default for RayTracerImpl<R> {
    fn default() -> Self {
        Self {
            light_trace_depth: None,
            eye_trace_depth: None,

            min_num_samples: 10,
            max_num_samples: 500,
            converge_abs_tol: 0.05,

            rng: R::default(),
        }
    }
}

impl<R: math::RandomSample> RayTracerImpl<R> {
    /// Perform a ray trace for a single input ray.
    pub fn trace(&self, scene: &sc::Scene, eye_ray: math::Ray) -> Float {
        let mut stats_stream = math::StatsStream::default();
        for sample_index in 0..self.max_num_samples.max(self.min_num_samples) {
            let (eye_trace, light_trace) = self.run_path_traces(scene, &eye_ray);
            let intensity_calculator = self.eval_trace(scene, &eye_trace, &light_trace);
            let intensity = intensity_calculator.calculate();
            if intensity.is_nan() {
                panic!("NaN encountered: {:#?}", intensity);
            }
            stats_stream.append(intensity);
            if sample_index >= self.min_num_samples
                && ((stats_stream.mean() - 2. * stats_stream.standard_error()) * scene.exposure
                    > 1.
                    || stats_stream.standard_error() * scene.exposure < self.converge_abs_tol)
            {
                break;
            }
        }
        stats_stream.mean()
    }

    // Setters.
    pub fn with_light_trace_depth(self, light_trace_depth: Option<usize>) -> Self {
        Self {
            light_trace_depth,
            ..self
        }
    }
    pub fn with_eye_trace_depth(self, eye_trace_depth: Option<usize>) -> Self {
        if let Some(depth) = eye_trace_depth {
            if depth < 2 {
                panic!("`eye_trace_depth` must be at least 2");
            }
        }
        Self {
            eye_trace_depth,
            ..self
        }
    }
    pub fn with_num_samples(self, num_samples: usize) -> Self {
        Self {
            min_num_samples: num_samples,
            max_num_samples: num_samples,
            ..self
        }
    }
    pub fn with_min_num_samples(self, min_num_samples: usize) -> Self {
        Self {
            min_num_samples,
            ..self
        }
    }
    pub fn with_max_num_samples(self, max_num_samples: usize) -> Self {
        Self {
            max_num_samples,
            ..self
        }
    }
    pub fn with_converge_abs_tol(self, converge_abs_tol: Float) -> Self {
        Self {
            converge_abs_tol,
            ..self
        }
    }

    /// Compute `Trace`s for both an eye path and light path.
    pub fn run_path_traces<'a>(
        &self,
        scene: &'a sc::Scene,
        eye_ray: &math::Ray,
    ) -> (Trace<'a>, Trace<'a>) {
        let eye_trace = self.run_path_trace(
            scene,
            TraceNode::Eye {
                exitant_ray: eye_ray.clone(),
            },
            false,
        );
        let make_light_trace = || {
            self.run_path_trace(
                scene,
                TraceNode::Light {
                    emit_sample: scene.sample_emitting_point(&self.rng),
                },
                true,
            )
        };
        let light_trace = if let Some(light_trace_depth) = self.light_trace_depth {
            if light_trace_depth == 0 {
                Trace::default()
            } else {
                make_light_trace()
            }
        } else {
            make_light_trace()
        };

        (eye_trace, light_trace)
    }

    fn run_path_trace<'a>(
        &self,
        scene: &'a sc::Scene,
        initial_node: TraceNode<'a>,
        is_light_trace: bool,
    ) -> Trace<'a> {
        let mut trace = Trace::default();
        trace.nodes.push(initial_node.clone());

        let adjoint = matches!(initial_node, TraceNode::Eye { .. });

        let mut depth: usize = 1;
        loop {
            let last_node: &TraceNode = trace.nodes.last().unwrap();

            // Sample to determine the next direction.
            //
            // Sampling never fails (other than from a `Terminal` node), since even if we
            // generate an occluded path we'll simply take the function value to be zero
            // (due to the visibility test factor). Ideally we'd sample in such a way as
            // to never generate an occluded path, but this is very difficult in practice,
            // so we instead simply use "local" sampling with ray tracing and accept
            // generating some occluded paths to be rejected with the visibility test
            // factor. Using rejection sampling doesn't work because we need to know the
            // probability density value for the importance sampling estimate.
            if let Some((importance_sample, prev_exitant_ray)) =
                last_node.sample(adjoint, &self.rng)
            {
                // "Russian roulette" logic.
                let trace_continue_prob = self.compute_trace_continue_prob(
                    depth,
                    importance_sample.quotient(),
                    is_light_trace,
                );
                if self.rng.random_float() > trace_continue_prob {
                    break;
                }

                // Perform ray cast.
                if let Some(collision) = scene.ray_cast(&prev_exitant_ray.nudge_along_by(EPSILON)) {
                    // If the ray cast was successful, make the next trace node.
                    let propagation_factor = compute_propagation_factor(
                        scene,
                        (last_node.point(), last_node.normal()),
                        (&collision.point, &collision.normal.dir),
                        true,
                        false,
                    );
                    let base_factor_partial_product = importance_sample
                        * last_node.factor_partial_product()
                        * propagation_factor.mul_prob_density_by(trace_continue_prob);
                    let next_node = TraceNode::make_next(
                        scene,
                        &prev_exitant_ray,
                        base_factor_partial_product,
                        collision.object,
                        collision.point,
                        collision.normal,
                    );
                    trace.nodes.push(next_node);
                } else {
                    panic!("Ray went to infinity: {:?}", prev_exitant_ray);
                }
            } else {
                break;
            }

            depth += 1;
        }

        trace
    }

    /// Evaluate a pair of eye and light path `Trace`s, producing an
    /// `IntensityCalculator`. Exposed publicly for debugging.
    pub fn eval_trace<'a>(
        &self,
        scene: &sc::Scene,
        eye_trace: &Trace<'a>,
        light_trace: &Trace<'a>,
    ) -> IntensityCalculator {
        let eye_trace_len = eye_trace.nodes.len();
        let light_trace_len = {
            let mut light_trace_len = light_trace.nodes.len();
            // Check for a final `Terminal` node on the light trace. Ignore if there is one.
            if matches!(light_trace.nodes.last(), Some(TraceNode::Terminal { .. })) {
                light_trace_len -= 1;
            }
            light_trace_len
        };

        // Define the path length to be the number of nodes on a path.
        let max_path_len = if matches!(eye_trace.nodes.last(), Some(TraceNode::Terminal { .. })) {
            (eye_trace_len + light_trace_len - 1).max(eye_trace_len)
        } else {
            eye_trace_len + light_trace_len
        };
        assert!(
            max_path_len >= 2,
            "The total number of eye and light nodes must be at least 2"
        );

        let mut intensity_calculator = IntensityCalculator::default();

        // Handle the special case of a 2-length path, i.e. looking directly at a light
        // source.
        if let Some(TraceNode::Terminal { emit_sample, .. }) = eye_trace.nodes.get(1) {
            let exitant_ray = if let Some(TraceNode::Eye { exitant_ray }) = eye_trace.nodes.get(0) {
                exitant_ray
            } else {
                panic!("Eye traces must always start with an `Eye` node")
            };
            let exitant_dot_normal = -exitant_ray.dir.dot(&emit_sample.normal.outward_dir());
            let term = intensity_calculator.add_term(2);
            term.add_sample(
                SamplingStrategy {
                    eye_len: 2,
                    light_len: 0,
                },
                emit_sample.emit_profile.eval(exitant_dot_normal),
                vec![math::DeltaFactor::unity()],
            );
            return intensity_calculator;
        }

        // Iterate over all intensity integral terms in the Neumann series solution.
        for path_len in 3..=max_path_len {
            let term = intensity_calculator.add_term(path_len);

            // Iterate over all sampling strategies for this integral. There's a test
            // specifically checking this indexing math.
            let min_eye_prefix_len = path_len.checked_sub(light_trace_len).unwrap_or(0).max(2);
            let mut max_eye_prefix_len = eye_trace_len.min(path_len);
            if matches!(
                eye_trace.nodes.get(max_eye_prefix_len - 1),
                Some(TraceNode::Terminal { .. }),
            ) {
                if eye_trace_len < path_len {
                    // There's a terminal node at the end of the prefix, but the prefix alone isn't
                    // long enough to be a path, so the terminal node shouldn't be used.
                    max_eye_prefix_len = max_eye_prefix_len.min(eye_trace_len - 1);
                }
            } else {
                // No terminal node at the end of the prefix, so need at least one light node.
                max_eye_prefix_len = max_eye_prefix_len.min(path_len - 1);
            }
            for eye_len in min_eye_prefix_len..=max_eye_prefix_len {
                // Assemble the eye prefix.
                let eye_prefix = &eye_trace.nodes[..eye_len];

                // Assemble the light suffix.
                let light_len = path_len - eye_len;
                if let Some(TraceNode::Terminal { .. }) = eye_prefix.last() {
                    assert!(light_len == 0);
                }
                let light_suffix = &light_trace.nodes[..light_len];

                // Compute the importance sample for this sampling strategy.
                let sample_value =
                    compute_intensity(scene, light_suffix.last(), eye_prefix.last().unwrap());

                // Compute probability densities under other sampling strategies for multiple
                // importance sampling.
                let prob_densities = (min_eye_prefix_len..=max_eye_prefix_len)
                    .map(|strat_eye_prefix_len| {
                        compute_prob_density(eye_prefix, &light_suffix, strat_eye_prefix_len)
                    })
                    .collect();

                term.add_sample(
                    SamplingStrategy { eye_len, light_len },
                    sample_value,
                    prob_densities,
                )
            }
        }

        intensity_calculator
    }

    /// Compute the probability for continuing a trace (i.e. the Russian
    /// roulette probability).
    fn compute_trace_continue_prob(
        &self,
        depth: usize,
        factor: Float,
        is_light_trace: bool,
    ) -> Float {
        let trace_depth = if is_light_trace {
            self.light_trace_depth
        } else {
            self.eye_trace_depth
        };
        if let Some(max_depth) = trace_depth {
            if depth >= max_depth {
                return 0.;
            }
        }
        if depth <= 3 {
            // Make the continuing probability 1 for the first few nodes.
            1.
        } else {
            factor.min(1.)
        }
    }
}

// Compute the sample intensity for a given shadow ray. If `light_path_node` is
// `None`, then `eye_path_node` must be a `Terminal`.
fn compute_intensity(
    scene: &sc::Scene,
    light_path_node: Option<&TraceNode>,
    eye_path_node: &TraceNode,
) -> Float {
    match eye_path_node {
        TraceNode::Surface {
            point: eye_point,
            incident_ray: eye_incident_ray,
            scattering_params: eye_scattering_params,
            normal: eye_normal,
            factor_partial_product: eye_factor_partial_product,
            ..
        } => {
            match light_path_node
                .expect("Invalid pairing of a `Surface` eye node with no light node")
            {
                TraceNode::Surface {
                    point: light_point,
                    incident_ray: light_incident_ray,
                    scattering_params: light_scattering_params,
                    normal: light_normal,
                    factor_partial_product: light_factor_partial_product,
                    ..
                } => {
                    let light_to_eye_node =
                        math::UnitVector::new_normalize(eye_point - light_point);

                    eye_factor_partial_product.quotient()
                        * eye_scattering_params
                            .eval(eye_normal, &(-light_to_eye_node), &eye_incident_ray.dir)
                            .unwrap_value()
                        * compute_propagation_factor(
                            scene,
                            (eye_point, &eye_normal.dir),
                            (light_point, &light_normal.dir),
                            false,
                            true,
                        )
                        .quotient()
                        * light_factor_partial_product.quotient()
                        * light_scattering_params
                            .eval(light_normal, &light_to_eye_node, &light_incident_ray.dir)
                            .unwrap_value()
                }
                TraceNode::Light {
                    emit_sample:
                        sc::EmitSample {
                            point: light_point,
                            normal: light_normal,
                            emit_profile,
                            area_prob_dens,
                        },
                } => {
                    let light_to_eye_node =
                        math::UnitVector::new_normalize(eye_point - light_point);

                    eye_factor_partial_product.quotient()
                        * eye_scattering_params
                            .eval(eye_normal, &(-light_to_eye_node), &eye_incident_ray.dir)
                            .unwrap_value()
                        * compute_propagation_factor(
                            scene,
                            (eye_point, &eye_normal.dir),
                            (&light_point, &light_normal.dir),
                            false,
                            true,
                        )
                        .quotient()
                        * emit_profile.eval(light_normal.outward_dir().dot(&light_to_eye_node))
                        / area_prob_dens
                }
                _ => {
                    panic!(
                        "Expected a `TraceNode::Surface` or `TraceNode::Light` on light \
                            path, found: {:?}",
                        light_path_node
                    );
                }
            }
        }
        TraceNode::Terminal {
            factor_partial_product,
            ..
        } => {
            assert!(light_path_node.is_none());
            factor_partial_product.quotient()
        }
        _ => {
            panic!(
                "Expected a `Surface` or `Terminal` on eye path, found: {:?}",
                eye_path_node
            );
        }
    }
}

/// Compute the probability density of a particular path under a particular
/// sampling strategy.
///
/// # Arguments
///
/// * `eye_prefix`: The set of nodes comprising the eye-path prefix.
/// * `light_prefix`: The set of nodes comprising the light-path prefix.
/// * `strat_eye_prefix_len`: The length of the eye prefix for the sampling
///   strategy to compute a probability density under.
fn compute_prob_density(
    eye_prefix: &[TraceNode],
    light_suffix: &[TraceNode],
    strat_eye_prefix_len: usize,
) -> math::DeltaFactor {
    let strat_light_prefix_len = eye_prefix.len() + light_suffix.len() - strat_eye_prefix_len;

    let mut prob_density = math::DeltaFactor::unity();
    for (eye_or_light, light_or_eye, strat_prefix_or_suffix_len) in [
        (eye_prefix, light_suffix, strat_eye_prefix_len),
        (light_suffix, eye_prefix, strat_light_prefix_len),
    ] {
        if let Some(node) = eye_or_light
            .len()
            .min(strat_prefix_or_suffix_len)
            .checked_sub(1)
            .and_then(|index| eye_or_light.get(index))
        {
            prob_density *= node.factor_partial_product().prob_density();
        }
        // A "reversing" node is one where we're reversing the causality across it when
        // computing probabilities under a different sampling strategy from the one used
        // to generate the path.
        for reversing_node_index in strat_prefix_or_suffix_len..eye_or_light.len() {
            let node = &eye_or_light[reversing_node_index];
            let mut already_used_one_light_or_eye_node = false;
            let maybe_prev_node = eye_or_light.get(reversing_node_index + 1).or_else(|| {
                already_used_one_light_or_eye_node = true;
                light_or_eye.last()
            });
            let prev_node = if let Some(prev_node) = maybe_prev_node {
                prev_node
            } else {
                prob_density *= if let TraceNode::Terminal { emit_sample, .. } = node {
                    emit_sample.area_prob_dens.into()
                } else {
                    panic!("Node should be a `Terminal`: {:#?}", node)
                };
                break;
            };
            let maybe_prev_prev_point = eye_or_light
                .get(reversing_node_index + 2)
                .map(|node| node.point())
                .or_else(|| {
                    light_or_eye
                        .len()
                        .checked_sub(if already_used_one_light_or_eye_node {
                            2
                        } else {
                            1
                        })
                        .and_then(|index| light_or_eye.get(index))
                        .map(|node| node.point())
                });
            prob_density *= prev_node
                .eval_sampling_prob_density(node.point(), maybe_prev_prev_point)
                * convert_to_area_prob_density(
                    (&prev_node.point(), &prev_node.normal()),
                    (&node.point(), &node.normal()),
                );
        }
    }

    prob_density
}

/// Compute the factor associated with propagating from one point to the next.
/// This includes the $G$ factor in Veach's notation as well as the conversion
/// factor from solid angle probability density to area probability density if
/// `convert_to_area_prob_dens` is enabled.
fn compute_propagation_factor(
    scene: &sc::Scene,
    (point, normal): (&math::Point, &math::UnitVector),
    (next_point, next_normal): (&math::Point, &math::UnitVector),
    convert_to_area_prob_dens: bool,
    check_visibility: bool,
) -> math::ImportanceSample {
    let displacement = next_point - point;
    let (exitant, separation) = math::UnitVector::new_and_get(displacement);

    let normal_dot_exitant = normal.dot(&exitant);
    let next_normal_dot_exitant = -next_normal.dot(&exitant);

    if check_visibility
        && (normal_dot_exitant < 0.
            || next_normal_dot_exitant < 0.
            || !scene.has_direct_line_of_sight(
                &(point + exitant.into_inner() * EPSILON),
                &(next_point - exitant.into_inner() * EPSILON),
            ))
    {
        return math::ImportanceSample::zero();
    }

    let geometric_factor =
        normal_dot_exitant.abs() * next_normal_dot_exitant.abs() / separation.powi(2);
    math::ImportanceSample::new(
        geometric_factor,
        if convert_to_area_prob_dens {
            math::DeltaFactor::from(geometric_factor)
        } else {
            math::DeltaFactor::unity()
        },
    )
}

/// Convert a projected solid angle probability density to a scene surface area
/// probability density.
fn convert_to_area_prob_density(
    (point, normal): (&math::Point, &math::UnitVector),
    (next_point, next_normal): (&math::Point, &math::UnitVector),
) -> math::DeltaFactor {
    let displacement = next_point - point;
    let (exitant, separation) = math::UnitVector::new_and_get(displacement);
    let normal_dot_exitant = normal.dot(&exitant);
    let next_normal_dot_exitant = -next_normal.dot(&exitant);
    math::DeltaFactor::from(
        normal_dot_exitant.abs() * next_normal_dot_exitant.abs() / separation.powi(2),
    )
}

#[derive(Clone, Debug, enum_as_inner::EnumAsInner)]
pub enum TraceNode<'a> {
    Eye {
        exitant_ray: math::Ray,
    },
    Light {
        emit_sample: sc::EmitSample<'a>,
    },
    Surface {
        incident_ray: math::Ray,
        point: math::Point,
        normal: math::OrientedNormal,
        factor_partial_product: math::ImportanceSample,
        scattering_params: &'a sc::ScatteringParams,
    },
    Terminal {
        emit_sample: sc::EmitSample<'a>,
        factor_partial_product: math::ImportanceSample,
    },
}

impl<'a> TraceNode<'a> {
    /// Get the location of the `TraceNode`.
    pub fn point(&self) -> &math::Point {
        match self {
            Self::Eye { exitant_ray, .. } => &exitant_ray.origin,
            Self::Surface { point, .. } => point,
            Self::Light {
                emit_sample: sc::EmitSample { point, .. },
                ..
            } => point,
            Self::Terminal {
                emit_sample: sc::EmitSample { point, .. },
                ..
            } => point,
        }
    }
    /// Get the normal of the `TraceNode`.
    pub fn normal(&self) -> &math::UnitVector {
        match self {
            Self::Eye { exitant_ray, .. } => &exitant_ray.dir,
            Self::Surface { normal, .. } => &normal.dir,
            Self::Light {
                emit_sample: sc::EmitSample { normal, .. },
                ..
            } => &normal.dir,
            Self::Terminal {
                emit_sample: sc::EmitSample { normal, .. },
                ..
            } => &normal.dir,
        }
    }
    /// Get the cumulative importance or intensity up until this node.
    pub fn factor_partial_product(&self) -> math::ImportanceSample {
        match self {
            TraceNode::Eye { .. } => math::ImportanceSample::unity(),
            TraceNode::Light { .. } => math::ImportanceSample::unity(),
            TraceNode::Surface {
                factor_partial_product,
                ..
            } => *factor_partial_product,
            TraceNode::Terminal {
                factor_partial_product,
                ..
            } => *factor_partial_product,
        }
    }

    /// Make the next `TraceNode` in a `Trace`.
    pub fn make_next(
        scene: &sc::Scene,
        incident_ray: &math::Ray,
        base_factor_partial_product: math::ImportanceSample,
        object: &'a sc::Object,
        point: math::Point,
        normal: math::OrientedNormal,
    ) -> Self {
        match &object.material.surface_params {
            sc::SurfaceParams::Scattering(scattering_params) => Self::Surface {
                incident_ray: incident_ray.clone(),
                point,
                normal,
                factor_partial_product: base_factor_partial_product,
                scattering_params,
            },
            sc::SurfaceParams::Emitting(emit_profile) => Self::Terminal {
                emit_sample: sc::EmitSample {
                    point,
                    normal,
                    area_prob_dens: scene.prob_of_picking_emitting_object(object)
                        * scene.compute_area_prob_density(object),
                    emit_profile,
                },
                factor_partial_product: base_factor_partial_product
                    * emit_profile.eval(-incident_ray.dir.dot(&normal.outward_dir())),
            },
        }
    }

    /// Determine the probability of selecting a point given a previous
    /// point.
    pub fn eval_sampling_prob_density(
        &self,
        next_point: &math::Point,
        maybe_prev_point: Option<&math::Point>,
    ) -> math::DeltaFactor {
        let handle_emit_sample = |emit_sample| {
            let &sc::EmitSample {
                point,
                normal,
                area_prob_dens,
                ..
            } = emit_sample;
            let exitant = math::UnitVector::new_normalize(next_point - point);
            let mu = exitant.dot(&normal.dir);
            if mu < 0. {
                math::DeltaFactor::zero()
            } else {
                math::DeltaFactor::from(area_prob_dens / (2. * math::PI) / exitant.dot(&normal.dir))
            }
        };
        match self {
            TraceNode::Eye { .. } => {
                panic!("Shouldn't need to compute probability densities from a `TraceNode::Eye`")
            }
            TraceNode::Light { emit_sample } => handle_emit_sample(emit_sample),
            TraceNode::Surface {
                point,
                normal,
                scattering_params,
                ..
            } => scattering_params.eval_sampling_prob_density(
                normal,
                &math::UnitVector::new_normalize(next_point - point),
                &math::UnitVector::new_normalize(
                    point - maybe_prev_point.expect("Previous point required"),
                ),
            ),
            TraceNode::Terminal { emit_sample, .. } => handle_emit_sample(emit_sample),
        }
    }
    /// Determine the next direction in which to ray cast, and also compute the
    /// associated intensity or importance.
    ///
    /// Returns `None` if attempting to sample from a `Terminal` node.
    pub fn sample(
        &self,
        adjoint: bool,
        rng: &impl math::RandomSample,
    ) -> Option<(math::ImportanceSample, math::Ray)> {
        Some(match self {
            Self::Eye { exitant_ray } => (math::ImportanceSample::unity(), exitant_ray.clone()),
            Self::Light { emit_sample } => {
                let normal = &emit_sample.normal.dir;
                let (exitant, importance_sample) =
                    emit_sample.emit_profile.importance_sample(normal, rng);
                (
                    importance_sample.mul_prob_density_by(emit_sample.area_prob_dens),
                    math::Ray::new(emit_sample.point, exitant),
                )
            }
            Self::Surface {
                incident_ray,
                point,
                normal,

                scattering_params,
                ..
            } => {
                let (exitant, brdf_value) =
                    scattering_params.importance_sample(&normal, &incident_ray.dir, adjoint, rng);
                (brdf_value, math::Ray::new(*point, exitant))
            }
            Self::Terminal { .. } => {
                return None;
            }
        })
    }
}

#[derive(Debug, Default)]
pub struct Trace<'a> {
    pub nodes: Vec<TraceNode<'a>>,
}

#[derive(Debug)]
pub struct SamplingStrategy {
    pub eye_len: usize,
    pub light_len: usize,
}

/// Data needed to produce an estimate of a single integral term in the overall
/// intensity Neumann series.
#[allow(dead_code)]
#[derive(Default, Debug)]
pub struct IntensityTermCalculator {
    /// The path length this term calculator applies to.
    path_len: usize,
    /// The value of the importance sample for each sampling strategy.
    samples: Vec<(SamplingStrategy, Float)>,
    /// The probability density of each sample stored in a 2D array.
    /// `sample_prob_densities_by_strategy_index[i][j]` is the probability
    /// density of the `i`th sample under the `j`th sampling strategy.
    prob_densities_by_sample_index: Vec<Vec<math::DeltaFactor>>,
}
impl IntensityTermCalculator {
    pub fn new(path_len: usize) -> Self {
        Self {
            path_len,
            samples: Vec::new(),
            prob_densities_by_sample_index: Vec::new(),
        }
    }
    pub fn add_sample(
        &mut self,
        strategy: SamplingStrategy,
        value: Float,
        prob_densities: Vec<math::DeltaFactor>,
    ) {
        self.samples.push((strategy, value));
        self.prob_densities_by_sample_index.push(prob_densities);
    }
    pub fn calculate(self) -> Float {
        let mut integral: Float = 0.;
        for (i, (_, sample)) in self.samples.into_iter().enumerate() {
            let norm: math::DeltaFactor = self.prob_densities_by_sample_index[i]
                .iter()
                .map(|p| p.powi(2))
                .sum();
            let weight = (self.prob_densities_by_sample_index[i][i].powi(2) / norm).unwrap_value();
            integral += weight * sample;
        }
        integral
    }
}

#[derive(Default, Debug)]
pub struct IntensityCalculator {
    intensity_term_calculators: Vec<IntensityTermCalculator>,
}
impl IntensityCalculator {
    pub fn add_term(&mut self, path_len: usize) -> &mut IntensityTermCalculator {
        self.intensity_term_calculators
            .push(IntensityTermCalculator::new(path_len));
        self.intensity_term_calculators.last_mut().unwrap()
    }
    pub fn calculate(self) -> Float {
        self.intensity_term_calculators
            .into_iter()
            .map(|intensity_term_calculator| intensity_term_calculator.calculate())
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    use nalgebra as na;

    use super::*;
    use crate::{math, scene as sc, test_util as util};

    const BOX_HALF_WIDTH: Float = 4.;
    const REFLECT_WEIGHT: Float = 0.9;

    type MockRayTracer = DeterministicRayTracer;

    /// Ensure that the probabilities generated with `TraceNode::sample` and
    /// `TraceNode::eval_sampling_prob_density` match.
    #[test]
    fn test_sample_vs_eval_prob_density() {
        let emit_profile = math::EmitProfile::new([(0, 1.), (2, 0.23)]);
        let normal = math::OrientedNormal {
            dir: math::Vector::x_axis(),
            is_outward: true,
        };
        let emit_sample = sc::EmitSample {
            point: math::Point::new(0., 0., 0.),
            normal,
            area_prob_dens: 0.25,
            emit_profile: &emit_profile,
        };
        let node = TraceNode::Light { emit_sample };
        let rng = math::DeterministicRandomEngine::default();
        for _ in 0..20 {
            let (importance_sample, ray) = node.sample(true, &rng).unwrap();
            assert_relative_eq!(
                emit_profile.eval(normal.dot(&ray.dir)),
                importance_sample.value().unwrap_value()
            );
            assert_relative_eq!(
                importance_sample.prob_density().factor(),
                node.eval_sampling_prob_density(&ray.nudge_along_by(2.).origin, None)
                    .unwrap_value()
            );
        }
    }

    /// Check that the math used to index first by the path length and second by
    /// the length of the eye prefix is correct.
    #[test]
    fn test_indexing() {
        // * `n_e`: Number of nodes in eye trace.
        for n_e in [2u32, 3, 7, 20] {
            // * `n_l`: Number of nodes in light trace.
            for n_l in [1u32, 2, 4, 7, 20] {
                let mut values_1 = std::collections::BTreeSet::new();
                // * `s`: Number of nodes in eye prefix.
                for s in 2..=n_e {
                    // * `t`: Number of nodes in light prefix.
                    for t in 1..=n_l {
                        // * `p`: Length of path.
                        let p = s + t;
                        values_1.insert([s, t, p]);
                    }
                }

                let mut values_2 = std::collections::BTreeSet::new();
                for p in 3..=n_e + n_l {
                    for s in 2u32.max(p.checked_sub(n_l).unwrap_or(0))..=n_e.min(p - 1) {
                        let t = p - s;
                        values_2.insert([s, t, p]);
                    }
                }

                assert_eq!(values_1, values_2);
            }
        }
    }

    #[test]
    fn test_spec_hit_balls() {
        let emit_intensity = 1.92834;
        let scene = build_spec_scene(Some(emit_intensity));
        let ray_tracer = MockRayTracer::default().with_eye_trace_depth(Some(5));

        // Send ray towards emitting ball at (1, 1, 1).
        let ray = util::make_ray([0.; 3], [1., 1., 1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, emit_intensity, epsilon = 1e-3);

        // Send ray towards ball at (-1, -1, -2).
        let ray = util::make_ray([0.; 3], [-1., -1., -1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, 0.);
    }

    #[test]
    fn test_spec_into_infinity() {
        let scene = build_spec_scene(None);
        let ray_tracer = MockRayTracer::default().with_eye_trace_depth(Some(5));

        // Send ray to infinity.
        let ray = util::make_ray([100.; 3], [1., 1., 1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, 0.);
    }

    #[test]
    fn test_spec_hit_mirrors() {
        let scene = build_spec_scene(None);
        let ray_tracer = DeterministicRayTracer::default()
            .with_converge_abs_tol(0.01)
            .with_max_num_samples(10000);

        // Send ray almost perpendicularly towards top reflecting wall of box, but with
        // enough x and y component so that it will eventually hit the source
        // ball.
        let dxy: Float = 0.012;
        let ray = util::make_ray([0.; 3], [dxy, dxy, 1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, REFLECT_WEIGHT.powf(4.), epsilon = 2e-2);
    }

    fn build_spec_scene(emit_intensity: Option<Float>) -> sc::Scene {
        // Two radius-1 balls, one emitting at (1, 1, 1) and one absorbing at (-1, -1,
        // -2).
        let mut objects = vec![
            sc::Object {
                shape: sc::Shape::make_ball(1.),
                isometry: na::Isometry3::translation(1., 1., 1.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting({
                    let mut profile = math::EmitProfile::identity();
                    if let Some(emit_intensity) = emit_intensity {
                        profile = profile * emit_intensity;
                    }
                    profile
                })),
            },
            sc::Object {
                shape: sc::Shape::make_ball(1.),
                isometry: na::Isometry3::translation(-1., -1., -2.),
                material: sc::Material::absorbing(),
            },
            // "Infinity sphere".
            sc::Object {
                shape: sc::Shape::make_ball(1000.),
                isometry: na::Isometry3::identity(),
                material: sc::Material::absorbing(),
            },
        ];

        // A cube of reflective planar walls, each placed at ±`BOX_HALF_WIDTH`.
        objects.push(sc::Object {
            shape: sc::Shape::make_cuboid(math::Vector::new(
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
            )),
            isometry: math::Isometry::identity(),
            material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                sc::ScatteringParams::specular(REFLECT_WEIGHT),
            )),
        });

        sc::Scene::from_objects(objects)
    }

    #[test]
    fn test_diff_reflect_planes_pure_emitting() {
        let ray_tracer = DeterministicRayTracer::default()
            .with_eye_trace_depth(Some(4))
            .with_num_samples(2);

        for x_comp in [0., 1.].iter().cloned() {
            let scene = build_diff_reflect_planes_scene(false);
            // Point at the emitting plane.
            {
                let ray = util::make_ray([0.; 3], [x_comp, 0., -1.]);
                let value = ray_tracer.trace(&scene, ray);
                assert_relative_eq!(value, 1.);
            }
            // Point at the diffusely reflecting plane.
            {
                let ray = util::make_ray([0.; 3], [x_comp, 0., 1.]);
                let value = ray_tracer.trace(&scene, ray);
                assert_relative_eq!(value, 1., epsilon = 0.1);
            }
        }
    }

    #[test]
    fn test_diff_reflect_planes_half_opaque() {
        let ray_tracer = DeterministicRayTracer::default()
            .with_eye_trace_depth(Some(4))
            .with_num_samples(1000);

        let scene = build_diff_reflect_planes_scene(true);

        // Send a ray towards the emitting side. Expect 1.
        let ray = util::make_ray([0.; 3], [1., 0., -1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, 1., epsilon = 0.05);

        // Send a ray towards the absorbing side. Expect 0.
        let ray = util::make_ray([0.; 3], [-1., 0., -1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, 0., epsilon = 0.05);

        // Send a ray towards the diffuse reflecting plane. Expect 1/2.
        let ray = util::make_ray([0.; 3], [0., 0., 1.]);
        let value = ray_tracer.trace(&scene, ray);
        assert_relative_eq!(value, 0.5, epsilon = 0.05);
    }

    /// Build a scene to test diffuse reflection. A diffusely reflecting x-y
    /// plane at z=+1 and a parallel emitting plane at z=-1, optionally half
    /// opaque.
    fn build_diff_reflect_planes_scene(half_emit_plane: bool) -> sc::Scene {
        let mut objects = Vec::new();
        // Absorbing infinity sphere.
        objects.push(sc::Object {
            shape: sc::Shape::make_ball(2000.),
            isometry: na::Isometry3::identity(),
            material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                sc::ScatteringParams::matte(0.),
            )),
        });
        let plane = sc::Shape::make_plane(&[
            math::Vector::new(1000., 0., 0.),
            math::Vector::new(0., 1000., 0.),
        ]);
        {
            let upper_diff_refl_plane = sc::Object {
                shape: plane.clone(),
                isometry: na::Isometry3::translation(0., 0., 1.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                    sc::ScatteringParams::matte(1.),
                )),
            };
            let lower_emit_plane = sc::Object {
                shape: plane.clone(),
                isometry: na::Isometry3::translation(0., 0., -1.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                    math::EmitProfile::identity(),
                )),
            };
            objects.push(upper_diff_refl_plane);
            objects.push(lower_emit_plane);
        };
        if half_emit_plane {
            // Add an absorbing plane half covering the lower emitting plane. Accomplish
            // this by tilting its normal slightly in the x direction.
            let lower_absorb_plane = sc::Object {
                shape: plane.clone(),
                isometry: math::Isometry::new(
                    math::Vector::new(0., 0., -1.),
                    math::y_hat() * EPSILON,
                ),
                material: sc::Material::absorbing(),
            };
            objects.push(lower_absorb_plane);
        }

        sc::Scene::from_objects(objects)
    }

    #[test]
    fn test_diff_reflect_concentric_balls_trad_path_tracing_direct() {
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default()
                .with_eye_trace_depth(Some(3))
                .with_light_trace_depth(Some(0))
                .with_num_samples(10000),
            true,
        );
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default()
                .with_eye_trace_depth(Some(2))
                .with_light_trace_depth(Some(1))
                .with_num_samples(500),
            true,
        );
    }

    #[test]
    fn test_diff_reflect_concentric_balls_trad_path_tracing_indirect() {
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default()
                .with_eye_trace_depth(Some(20))
                .with_light_trace_depth(Some(0))
                .with_num_samples(5000),
            false,
        );
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default()
                .with_eye_trace_depth(None)
                .with_light_trace_depth(Some(0))
                .with_num_samples(5000),
            false,
        );
    }

    #[test]
    fn test_diff_reflect_concentric_balls_bidir_path_tracing_direct() {
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default()
                .with_eye_trace_depth(Some(2))
                .with_light_trace_depth(Some(1))
                .with_num_samples(1000),
            true,
        );
    }

    #[test]
    fn test_diff_reflect_concentric_balls_bidir_path_tracing_indirect() {
        test_diff_reflect_concentric_balls_impl(
            DeterministicRayTracer::default().with_converge_abs_tol(0.001),
            false,
        );
    }

    fn test_diff_reflect_concentric_balls_impl(
        ray_tracer: DeterministicRayTracer,
        direct_lighting: bool,
    ) {
        let albedo = 0.8;
        let inner_radius = 1.;
        let outer_radius = 10.;
        let emitted_intensity = 1.;

        let objects = vec![
            sc::Object {
                isometry: math::Isometry::identity(),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                    math::EmitProfile::isotropic(emitted_intensity),
                )),
                shape: sc::Shape::make_ball(inner_radius),
            },
            sc::Object {
                isometry: math::Isometry::identity(),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                    sc::ScatteringParams::matte(albedo),
                )),
                shape: sc::Shape::make_ball(outer_radius),
            },
        ];
        let scene = sc::Scene::from_objects(objects);

        // Send ray towards inner sphere.
        {
            let origin = math::Point::new(2., 0., 0.);
            let dir = -math::Vector::x_axis();
            assert_relative_eq!(1., ray_tracer.trace(&scene, math::Ray { origin, dir }));
        }
        // Send ray towards outer sphere.
        {
            let origin = math::Point::new(2., 0., 0.);
            let dir = math::Vector::x_axis();
            let base_expected_intensity =
                albedo * emitted_intensity * (inner_radius / outer_radius).powi(2);
            let expected_intensity = if direct_lighting {
                base_expected_intensity
            } else {
                base_expected_intensity
                    / (1. - albedo * (1. - (inner_radius / outer_radius).powi(2)))
            };
            assert_relative_eq!(
                expected_intensity,
                ray_tracer.trace(&scene, math::Ray { origin, dir }),
                epsilon = expected_intensity / 10.
            );
        }
    }

    #[test]
    fn test_diff_reflect_moon() {
        let moon_radius = 0.5;
        let moon_height = 6.;
        let moon_emit_intensity = 0.7;
        let ground_albedo = 0.3;

        let half_angle_rad = (moon_radius as Float / moon_height).atan();
        let moon_solid_angle_ster = 2. * math::PI * (1. - half_angle_rad.cos());

        let expected_intensity =
            moon_emit_intensity * ground_albedo * (moon_solid_angle_ster / math::PI);

        let objects = vec![
            // Moon.
            sc::Object {
                shape: sc::Shape::make_disc(moon_radius, -math::Vector::z_axis()),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                    math::EmitProfile::isotropic(moon_emit_intensity),
                )),
                isometry: math::Isometry::translation(0., 0., moon_height),
            },
            // Ground.
            sc::Object {
                shape: sc::Shape::make_plane(&[math::x_hat(), math::y_hat()]),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                    sc::ScatteringParams::matte(ground_albedo),
                )),
                isometry: math::Isometry::identity(),
            },
            // Infinity sphere.
            sc::Object {
                shape: sc::Shape::make_ball(100.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                    sc::ScatteringParams::matte(0.),
                )),
                isometry: math::Isometry::identity(),
            },
        ];
        let scene = sc::Scene::from_objects(objects);

        assert_relative_eq!(
            DeterministicRayTracer::default()
                .with_light_trace_depth(Some(1))
                .with_eye_trace_depth(Some(3))
                .with_num_samples(10000)
                .trace(
                    &scene,
                    math::Ray::new(math::Point::new(0., 0., 3.), -math::Vector::z_axis()),
                ),
            expected_intensity,
            epsilon = 1e-4
        );
        assert_relative_eq!(
            DeterministicRayTracer::default()
                .with_light_trace_depth(Some(0))
                .with_eye_trace_depth(Some(3))
                .with_num_samples(10000)
                .trace(
                    &scene,
                    math::Ray::new(math::Point::new(0., 0., 3.), -math::Vector::z_axis()),
                ),
            expected_intensity,
            epsilon = 5e-4
        );
    }

    /// Ensure `compute_area_prob_density` and `compute_propagation_factor` are
    /// consistent.
    #[test]
    fn test_convert_to_area_prob_density() {
        let point = math::Point::new(0., 0., 0.);
        let normal = math::Vector::y_axis();
        let next_point = math::Point::new(1., 1., 1.);
        let next_normal = -math::Vector::y_axis();
        let scene = sc::Scene::from_objects(vec![]);

        let prob_density_1 =
            convert_to_area_prob_density((&point, &normal), (&next_point, &next_normal));
        let prob_density_2 = compute_propagation_factor(
            &scene,
            (&point, &normal),
            (&next_point, &next_normal),
            true,
            false,
        )
        .prob_density();

        assert_relative_eq!(prob_density_1.factor(), prob_density_2.factor());
    }
}
