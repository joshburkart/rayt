use lazy_static::lazy_static;
use nalgebra as na;
use ncollide3d as nc;

pub use crate::{Float, PI};

const EPSILON: Float = 1e-7;

// Convenience aliases.
pub type Point = na::Point3<Float>;
pub type Vector = na::Vector3<Float>;
pub type UnitVector = na::Unit<Vector>;
pub type Isometry = na::Isometry3<Float>;

#[derive(Clone, Debug)]
pub struct Ray {
    pub origin: Point,
    pub dir: UnitVector,
}
impl Ray {
    pub fn new(origin: Point, dir: UnitVector) -> Self {
        Self { origin, dir }
    }

    pub fn nudge_along_by(&self, distance: Float) -> Self {
        Self {
            origin: self.origin + distance * self.dir.into_inner(),
            dir: self.dir,
        }
    }
}
impl From<&Ray> for nc::query::Ray<Float> {
    fn from(ray: &Ray) -> nc::query::Ray<Float> {
        nc::query::Ray::<Float> {
            origin: ray.origin,
            dir: ray.dir.into_inner(),
        }
    }
}

/// Struct to represent a normal unit vector that also indicates whether it
/// points outward from the surface (`is_outward=true`) or inward into the
/// surface (`is_outward=false`).
#[derive(Clone, Copy, Debug)]
pub struct OrientedNormal {
    pub dir: UnitVector,
    pub is_outward: bool,
}
impl OrientedNormal {
    pub fn dot(&self, vector: &UnitVector) -> Float {
        self.dir.dot(vector)
    }
    pub fn outward_dir(&self) -> UnitVector {
        if self.is_outward {
            self.dir
        } else {
            -self.dir
        }
    }
}

pub fn x_hat() -> Vector {
    Vector::x_axis().into_inner()
}
pub fn y_hat() -> Vector {
    Vector::y_axis().into_inner()
}
pub fn z_hat() -> Vector {
    Vector::z_axis().into_inner()
}

/// Generate a translation.
pub fn translation(vector: Vector) -> Isometry {
    let translation = na::Translation3::from(vector);
    Isometry::from_parts(
        translation,
        na::Unit::new_unchecked(na::Quaternion::identity()),
    )
}

/// An angular emission profile represented as a power series in $\mu =
/// \cos(\theta)$.
#[derive(Debug, Clone)]
pub struct EmitProfile {
    /// The order of each monomial term mapped to its coefficient.
    order_to_coef: std::collections::HashMap<i32, Float>,
    /// The integral of the profile times $\mu$ (i.e. $\cos\theta$) over a
    /// hemisphere of solid angle.
    integral: Float,
}

impl EmitProfile {
    /// Make a new instance.
    pub fn new<II: IntoIterator<Item = (i32, Float)>>(order_to_coef: II) -> Self {
        let order_to_coef = order_to_coef.into_iter().collect();
        let integral = Self::compute_integral(&order_to_coef);
        Self {
            order_to_coef,
            integral,
        }
    }
    /// Make an isotropic profile.
    pub fn isotropic(value: Float) -> Self {
        Self::new([(0, value)])
    }
    /// Make an isotropic profile with unit value.
    pub fn identity() -> Self {
        Self::isotropic(1.)
    }
    /// Make a profile with a single term.
    pub fn single_term(order: i32, coef: Float) -> Self {
        Self::new([(order, coef)])
    }

    /// Get the integral of the profile times $\mu$ (i.e. $\cos\theta$) over a
    /// hemisphere of solid angle.
    pub fn integral(&self) -> Float {
        self.integral
    }

    /// Take a sample from the profile. Report projected probability density in
    /// the returned `ImportanceSample`.
    pub fn importance_sample(
        &self,
        normal: &UnitVector,
        rng: &impl RandomSample,
    ) -> (UnitVector, ImportanceSample) {
        let exitant = sample_from_hemisphere(&normal, rng);
        let exitant_dot_normal = exitant.dot(&normal);
        let sample = ImportanceSample::new(
            self.eval(exitant_dot_normal),
            1. / 2. / PI / exitant_dot_normal,
        );
        (exitant, sample)
    }

    /// Evaluate the profile at a given value of $\mu$ (i.e. $\cos\theta$).
    pub fn eval(&self, mu: Float) -> Float {
        if mu < 0. {
            0.
        } else {
            self.order_to_coef
                .iter()
                .map(|(order, coef)| coef * mu.powi(*order))
                .sum()
        }
    }

    /// Evaluate the integral of the profile times $\mu$ (i.e. $\cos\theta$)
    /// over a hemisphere of solid angle.
    fn compute_integral(order_to_coef: &std::collections::HashMap<i32, Float>) -> Float {
        2. * PI
            * order_to_coef
                .iter()
                .map(|(order, coef)| coef / (order + 2) as Float)
                .sum::<Float>()
    }
}

/// Allow multiplying an angular profile by a scalar.
impl std::ops::Mul<Float> for EmitProfile {
    type Output = EmitProfile;
    fn mul(self, rhs: Float) -> Self::Output {
        Self::new(
            self.order_to_coef
                .into_iter()
                .map(|(order, coef)| (order, coef * rhs)),
        )
    }
}

/// Allow dividing an angular profile by a scalar.
impl std::ops::Div<Float> for EmitProfile {
    type Output = EmitProfile;
    fn div(self, rhs: Float) -> Self::Output {
        self * (1. / rhs)
    }
}

/// A positive or zero floating point number (stored in log space) multiplied by
/// an integral number of solid angle delta function factors.
#[derive(Clone, Copy, Debug)]
pub struct DeltaFactor {
    log10_factor: Option<Float>,
    /// The number of (infinite) factors of a Dirac delta function evaluated at
    /// 0 that `value` contains.
    num_deltas: u16,
}
impl From<Float> for DeltaFactor {
    fn from(value: Float) -> Self {
        Self {
            log10_factor: Some(value.log10()),
            num_deltas: 0,
        }
    }
}
impl DeltaFactor {
    pub fn factor(&self) -> Float {
        if let Some(log10_value) = self.log10_factor {
            if log10_value > (f64::MIN_POSITIVE * 1e4).log10() {
                (10f64).powf(log10_value)
            } else {
                f64::MIN_POSITIVE * 1e4
            }
        } else {
            0.
        }
    }
    pub fn num_deltas(&self) -> u16 {
        self.num_deltas
    }
    pub fn unwrap_value(self) -> Float {
        if self.num_deltas != 0 {
            panic!(
                "Attempted to unwrap a `DeltaFactor` with nonzero number of delta factors: {:?}",
                self
            );
        }
        self.factor()
    }

    pub fn unity() -> Self {
        Self {
            log10_factor: Some(0.),
            num_deltas: 0,
        }
    }
    pub fn zero() -> Self {
        Self {
            log10_factor: None,
            num_deltas: 0,
        }
    }
    pub fn with_num_deltas(self, num_deltas: u16) -> Self {
        Self { num_deltas, ..self }
    }

    pub fn powi(&self, power: u16) -> Self {
        Self {
            log10_factor: if let Some(log10_value) = self.log10_factor {
                Some(power as Float * log10_value)
            } else {
                None
            },
            num_deltas: power * self.num_deltas,
        }
    }
}
impl std::ops::Mul for DeltaFactor {
    type Output = DeltaFactor;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            log10_factor: if let Some(lhs_log10_value) = self.log10_factor {
                if let Some(rhs_log10_value) = rhs.log10_factor {
                    Some(lhs_log10_value + rhs_log10_value)
                } else {
                    None
                }
            } else {
                None
            },
            num_deltas: self.num_deltas + rhs.num_deltas,
        }
    }
}
impl std::ops::Div for DeltaFactor {
    type Output = DeltaFactor;

    fn div(self, rhs: Self) -> Self::Output {
        Self {
            log10_factor: if let Some(lhs_log10_value) = self.log10_factor {
                if let Some(rhs_log10_value) = rhs.log10_factor {
                    Some(lhs_log10_value - rhs_log10_value)
                } else {
                    panic!("Divide by zero")
                }
            } else {
                None
            },
            num_deltas: self
                .num_deltas
                .checked_sub(rhs.num_deltas)
                .unwrap_or_else(|| {
                    panic!(
                        "Negative number of delta factors not supported: lhs: {:?}, rhs: {:?}",
                        self, rhs
                    )
                }),
        }
    }
}
impl std::ops::MulAssign for DeltaFactor {
    fn mul_assign(&mut self, rhs: Self) {
        *self = *self * rhs
    }
}
impl std::ops::Add for DeltaFactor {
    type Output = DeltaFactor;

    fn add(self, rhs: Self) -> Self::Output {
        if self.num_deltas == rhs.num_deltas {
            DeltaFactor::from(self.factor() + rhs.factor()).with_num_deltas(self.num_deltas)
        } else if self.num_deltas > rhs.num_deltas {
            self
        } else {
            rhs
        }
    }
}
impl std::iter::Sum for DeltaFactor {
    fn sum<I: Iterator<Item = Self>>(mut iter: I) -> Self {
        let mut total = None;
        while let Some(current) = iter.next() {
            total = Some(match total {
                Some(prev_total) => prev_total + current,
                None => current,
            })
        }
        total.expect("Empty iterator?")
    }
}
impl std::ops::Mul<Float> for DeltaFactor {
    type Output = DeltaFactor;

    fn mul(self, rhs: Float) -> Self::Output {
        Self {
            log10_factor: if let Some(log10_value) = self.log10_factor {
                Some(log10_value + rhs.log10())
            } else {
                None
            },
            ..self
        }
    }
}
impl std::ops::Mul<DeltaFactor> for Float {
    type Output = DeltaFactor;

    fn mul(self, other: DeltaFactor) -> DeltaFactor {
        other * self
    }
}
impl std::ops::Div<Float> for DeltaFactor {
    type Output = DeltaFactor;

    fn div(self, rhs: Float) -> Self::Output {
        Self {
            log10_factor: if let Some(lhs_log10_value) = self.log10_factor {
                Some(lhs_log10_value - rhs.log10())
            } else {
                None
            },
            ..self
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ImportanceSample {
    /// The value of the sampled function.
    value: DeltaFactor,
    /// The (possibly conditional) probability density associated with the
    /// sample.
    prob_density: DeltaFactor,
}
impl ImportanceSample {
    pub fn new<I1: Into<DeltaFactor>, I2: Into<DeltaFactor>>(value: I1, prob_density: I2) -> Self {
        Self {
            value: value.into(),
            prob_density: prob_density.into(),
        }
    }
    pub fn unity() -> Self {
        Self {
            value: DeltaFactor::unity(),
            prob_density: DeltaFactor::unity(),
        }
    }
    pub fn zero() -> Self {
        Self {
            value: DeltaFactor::zero(),
            prob_density: DeltaFactor::unity(),
        }
    }
    #[must_use]
    pub fn with_num_deltas(self, num_deltas: u16) -> Self {
        Self {
            value: self.value.with_num_deltas(num_deltas),
            prob_density: self.prob_density.with_num_deltas(num_deltas),
        }
    }
    #[must_use]
    pub fn mul_prob_density_by<I: Into<DeltaFactor>>(self, factor: I) -> Self {
        Self {
            value: self.value,
            prob_density: self.prob_density * factor.into(),
        }
    }

    pub fn value(&self) -> DeltaFactor {
        self.value
    }
    pub fn prob_density(&self) -> DeltaFactor {
        self.prob_density
    }
    pub fn quotient(&self) -> Float {
        let quotient = self.value / self.prob_density;
        if quotient.num_deltas != 0 {
            panic!("Attempted to take the quotient of an unbalanced `ImportanceSample`");
        }
        quotient.factor()
    }
}
impl std::ops::Mul for ImportanceSample {
    type Output = ImportanceSample;

    fn mul(self, other: Self) -> Self {
        Self {
            value: self.value * other.value,
            prob_density: self.prob_density * other.prob_density,
        }
    }
}
impl std::ops::Mul<Float> for ImportanceSample {
    type Output = ImportanceSample;

    fn mul(self, other: Float) -> Self {
        Self {
            value: other * self.value,
            ..self
        }
    }
}
impl std::ops::Mul<ImportanceSample> for Float {
    type Output = ImportanceSample;

    fn mul(self, other: ImportanceSample) -> ImportanceSample {
        other * self
    }
}
impl std::ops::Div<Float> for ImportanceSample {
    type Output = ImportanceSample;

    fn div(self, other: Float) -> Self {
        self * (1. / other)
    }
}

/// Trait for a bidirectional scattering distribution function.
pub trait BSDF {
    /// Produce a scalar representing the evaluated BSDF.
    fn eval(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor;
}

pub trait SamplingBSDF: BSDF {
    /// Generate an importance sample of the incident unit vector from the BSDF
    /// for use in Monte Carlo calculations. Also return the result of
    /// `eval` and the value of the probability density function used to
    /// generate the importance sample as the second tuple element. The PDF
    /// should be with respect to *projected* solid angle, i.e. including a
    /// factor of $1 / \cos(\theta_o)$ (see section 8.2.2.2 of Veach's thesis).
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample);

    /// Compute the value of the probability density function used to generate
    /// importance samples. The PDF should be with respect to *projected*
    /// solid angle, i.e. including a factor of $1 / \cos(\theta_o)$ (see
    /// section 8.2.2.2 of Veach's thesis).
    fn eval_sampling_prob_density(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor;
}

/// A BSDF for the modified Phong model. See e.g.
/// https://www.cs.princeton.edu/courses/archive/fall03/cs526/papers/lafortune94.pdf.
#[derive(Clone, Debug)]
pub struct GlossyBSDF {
    /// The specular exponent to use.
    spec_exponent: Float,
}

impl GlossyBSDF {
    pub fn new(fwhm_angle_rad: Float) -> Self {
        let spec_exponent = (0.5f64).log10() / fwhm_angle_rad.cos().log10();
        Self { spec_exponent }
    }
}
impl BSDF for GlossyBSDF {
    fn eval(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        let reflected_incident = reflect(&normal.dir, incident);
        let prefactor = (self.spec_exponent + 2.) / (2. * PI);
        let cos_exitant_reflected_incident = exitant.dot(&reflected_incident);
        if cos_exitant_reflected_incident < 0. {
            DeltaFactor::zero()
        } else {
            DeltaFactor::from(prefactor * cos_exitant_reflected_incident.powf(self.spec_exponent))
        }
    }
}
impl SamplingBSDF for GlossyBSDF {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        _adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        let reflected_incident = reflect(&normal.dir, incident);

        let rand = rng.random_float();
        let alpha = rand.powf(1. / (self.spec_exponent + 1.)).acos();
        let phi = rng.random_float() * 2. * PI;
        let [tangent, _] = make_random_tangent_ortho_basis(&reflected_incident, rng);
        let rotated_by_alpha =
            na::Rotation::from_axis_angle(&tangent, alpha).transform_vector(&reflected_incident);
        let exitant = UnitVector::new_normalize(
            na::Rotation::from_axis_angle(&reflected_incident, phi)
                .transform_vector(&rotated_by_alpha),
        );

        (
            exitant,
            ImportanceSample::new(
                self.eval(normal, incident, &exitant),
                (self.spec_exponent + 1.) / 2. / PI * alpha.cos().powf(self.spec_exponent)
                    / normal.dir.dot(&exitant).abs(),
            ),
        )
    }

    fn eval_sampling_prob_density(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        let reflected_incident = reflect(&normal.dir, incident);
        DeltaFactor::from(
            (self.spec_exponent + 1.) / 2. / PI
                * reflected_incident
                    .dot(exitant)
                    .abs()
                    .powf(self.spec_exponent)
                / normal.dir.dot(exitant).abs(),
        )
    }
}

#[derive(Clone, Debug)]
pub struct MatteBSDF {}

impl Default for MatteBSDF {
    fn default() -> Self {
        Self {}
    }
}

impl BSDF for MatteBSDF {
    fn eval(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        (1. / PI).into()
    }
}
impl SamplingBSDF for MatteBSDF {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        _adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        let rand = rng.random_float();
        let theta = rand.sqrt().acos();
        let phi = rng.random_float() * 2. * PI;
        let [tangent, _] = make_random_tangent_ortho_basis(&normal.dir, rng);
        let rotated_by_theta =
            na::Rotation::from_axis_angle(&tangent, theta).transform_vector(&normal.dir);
        let exitant = UnitVector::new_normalize(
            na::Rotation::from_axis_angle(&normal.dir, phi).transform_vector(&rotated_by_theta),
        );
        (
            exitant,
            ImportanceSample::new(self.eval(normal, incident, &exitant), 1. / PI),
        )
    }

    fn eval_sampling_prob_density(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        (1. / PI).into()
    }
}

/// A perfectly specular BSDF (not including Fresnel equations).
#[derive(Clone, Debug)]
pub struct SpecularBSDF {}

impl Default for SpecularBSDF {
    fn default() -> Self {
        Self {}
    }
}
impl BSDF for SpecularBSDF {
    fn eval(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        DeltaFactor::zero()
    }
}
impl SamplingBSDF for SpecularBSDF {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        _adjoint: bool,
        _rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        let exitant_sample = reflect(&normal.dir, incident);
        (
            exitant_sample,
            ImportanceSample::new(
                1. / normal.dir.dot(&exitant_sample).abs(),
                1. / normal.dir.dot(&exitant_sample).abs(),
            )
            .with_num_deltas(1),
        )
    }

    fn eval_sampling_prob_density(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        DeltaFactor::zero()
    }
}

/// A BSDF for refractive materials.
#[derive(Clone, Debug)]
pub struct RefractiveBSDF {
    index_of_refraction: Float,
}

impl RefractiveBSDF {
    pub fn new(index_of_refraction: Float) -> Self {
        Self {
            index_of_refraction,
        }
    }
}

impl BSDF for RefractiveBSDF {
    fn eval(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        DeltaFactor::zero()
    }
}
impl SamplingBSDF for RefractiveBSDF {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        let incident_normal_comp = incident.dot(&normal.dir);
        let incident_parallel_proj =
            incident.into_inner() - incident_normal_comp * normal.dir.into_inner();
        let n_ratio = if incident_normal_comp > 0. {
            self.index_of_refraction
        } else {
            1. / self.index_of_refraction
        };
        let n_ratio = if normal.is_outward {
            n_ratio
        } else {
            1. / n_ratio
        };

        let exitant_normal_comp_squared: Float =
            1. - n_ratio.powi(2) * (1. - incident_normal_comp.powi(2));
        if exitant_normal_comp_squared <= 0. {
            assert!(
                (normal.is_outward && incident_normal_comp >= 0.)
                    || (!normal.is_outward && incident_normal_comp <= 0.)
            );
            return SpecularBSDF {}.importance_sample(normal, incident, adjoint, rng);
        }
        let exitant_normal_comp = exitant_normal_comp_squared.sqrt();
        let exitant_parallel_proj = if incident_parallel_proj.norm() > EPSILON {
            (1. - exitant_normal_comp_squared).sqrt() * incident_parallel_proj
                / incident_parallel_proj.norm()
        } else {
            incident_parallel_proj
        };
        let exitant_sample = UnitVector::new_unchecked(
            exitant_parallel_proj
                + incident_normal_comp.signum() * normal.dir.into_inner() * exitant_normal_comp,
        );

        let factor = if adjoint { 1. } else { n_ratio.powi(2) };
        (
            exitant_sample,
            ImportanceSample::new(factor / exitant_normal_comp, 1. / exitant_normal_comp)
                .with_num_deltas(1),
        )
    }

    fn eval_sampling_prob_density(
        &self,
        _normal: &OrientedNormal,
        _incident: &UnitVector,
        _exitant: &UnitVector,
    ) -> DeltaFactor {
        DeltaFactor::zero()
    }
}

/// A weighted sum of two BSDFs.
#[derive(Clone, Debug)]
pub struct CombinedBSDF<B1: BSDF, B2: BSDF> {
    bsdf_1: B1,
    weight_1: Float,

    bsdf_2: B2,
    weight_2: Float,
}

impl<B1: BSDF, B2: BSDF> CombinedBSDF<B1, B2> {
    pub fn new(bsdf_1: B1, weight_1: Float, bsdf_2: B2, weight_2: Float) -> Self {
        Self {
            bsdf_1,
            weight_1,
            bsdf_2,
            weight_2,
        }
    }
}
impl<B1: BSDF, B2: BSDF> BSDF for CombinedBSDF<B1, B2> {
    fn eval(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        self.weight_1 * self.bsdf_1.eval(normal, incident, exitant)
            + self.weight_2 * self.bsdf_2.eval(normal, incident, exitant)
    }
}
impl<B1: SamplingBSDF, B2: SamplingBSDF> SamplingBSDF for CombinedBSDF<B1, B2> {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        fn handle_branch<BS: SamplingBSDF, BO: SamplingBSDF, RS: RandomSample>(
            (sampling_bsdf, sampling_weight): (&BS, Float),
            (other_bsdf, other_weight): (&BO, Float),
            normal: &OrientedNormal,
            incident: &UnitVector,
            adjoint: bool,
            rng: &RS,
        ) -> (UnitVector, ImportanceSample) {
            let (exitant, importance_sample) =
                sampling_bsdf.importance_sample(normal, incident, adjoint, rng);
            let other_bsdf_value = other_bsdf.eval(normal, incident, &exitant);
            let other_prob_density =
                other_bsdf.eval_sampling_prob_density(normal, incident, &exitant);
            (
                exitant,
                ImportanceSample::new(
                    sampling_weight * importance_sample.value() + other_weight * other_bsdf_value,
                    (sampling_weight * importance_sample.prob_density()
                        + other_weight * other_prob_density)
                        / (sampling_weight + other_weight),
                ),
            )
        }
        if rng.random_float() < self.weight_1 / (self.weight_1 + self.weight_2) {
            handle_branch(
                (&self.bsdf_1, self.weight_1),
                (&self.bsdf_2, self.weight_2),
                normal,
                incident,
                adjoint,
                rng,
            )
        } else {
            handle_branch(
                (&self.bsdf_2, self.weight_2),
                (&self.bsdf_1, self.weight_1),
                normal,
                incident,
                adjoint,
                rng,
            )
        }
    }

    fn eval_sampling_prob_density(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        (self.weight_1
            * self
                .bsdf_1
                .eval_sampling_prob_density(normal, incident, exitant)
            + self.weight_2
                * self
                    .bsdf_2
                    .eval_sampling_prob_density(normal, incident, exitant))
            / (self.weight_1 + self.weight_2)
    }
}

pub trait Combinable {
    fn combine_with<B2: SamplingBSDF>(
        self,
        self_weight: Float,
        other_bsdf: B2,
        other_weight: Float,
    ) -> CombinedBSDF<Self, B2>
    where
        Self: Sized + SamplingBSDF;
}

impl<B1: SamplingBSDF> Combinable for B1 {
    fn combine_with<B2: SamplingBSDF>(
        self,
        self_weight: Float,
        other_bsdf: B2,
        other_weight: Float,
    ) -> CombinedBSDF<B1, B2> {
        CombinedBSDF::<Self, B2>::new(self, self_weight, other_bsdf, other_weight)
    }
}

/// A scaled BSDF to allow for absorption (`scale < 1`) or fluorescence (`scale
/// > 1`).
#[derive(Clone, Debug)]
pub struct ScaledBSDF<B: SamplingBSDF> {
    bsdf: B,
    scale: Float,
}
impl<B: SamplingBSDF> ScaledBSDF<B> {
    pub fn new(bsdf: B, scale: Float) -> Self {
        Self { bsdf, scale }
    }
}
impl<B: SamplingBSDF> BSDF for ScaledBSDF<B> {
    fn eval(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        self.scale * self.bsdf.eval(normal, incident, exitant)
    }
}
impl<B: SamplingBSDF> SamplingBSDF for ScaledBSDF<B> {
    fn importance_sample<RS: RandomSample>(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        adjoint: bool,
        rng: &RS,
    ) -> (UnitVector, ImportanceSample) {
        let (sample, function_sample) = self.bsdf.importance_sample(normal, incident, adjoint, rng);
        (sample, function_sample * self.scale)
    }
    fn eval_sampling_prob_density(
        &self,
        normal: &OrientedNormal,
        incident: &UnitVector,
        exitant: &UnitVector,
    ) -> DeltaFactor {
        self.bsdf
            .eval_sampling_prob_density(normal, incident, exitant)
    }
}

/// Produce a uniformly (with respect to solid angle) random unit vector lying
/// in the hemisphere defined by the normal vector `normal`.
pub fn sample_from_hemisphere<RS: RandomSample>(normal: &UnitVector, rng: &RS) -> UnitVector {
    let [tangent_1, tangent_2] = make_random_tangent_ortho_basis(normal, rng);

    let rand_normal_vector = Vector::new(
        rng.random_sample(&*NORMAL_RNG_DIST),
        rng.random_sample(&*NORMAL_RNG_DIST),
        rng.random_sample(&*NORMAL_RNG_DIST).abs(),
    );
    let rand_normal_vector_norm = rand_normal_vector.norm();

    let cos_theta_rad = rand_normal_vector[2] / rand_normal_vector_norm;
    let sin_theta_rad = cos_theta_rad.acos().sin();
    let phi_rad = 2. * PI * rng.random_float();
    UnitVector::new_unchecked(
        sin_theta_rad * phi_rad.cos() * tangent_1.into_inner()
            + sin_theta_rad * phi_rad.sin() * tangent_2.into_inner()
            + cos_theta_rad * normal.into_inner(),
    )
}

pub fn make_tangent_orth_basis(normal: &UnitVector) -> [UnitVector; 2] {
    let tangent_1: UnitVector = {
        let mut non_parallel = Vector::zeros();
        let mut tangent = Vector::zeros();
        for axis in 0..3 {
            non_parallel[axis] = 1.;
            non_parallel /= non_parallel.norm();
            tangent = non_parallel.cross(normal);
            if tangent.norm() > EPSILON {
                break;
            } else {
                non_parallel[axis] = 0.;
            }
        }
        UnitVector::new_normalize(tangent)
    };
    let tangent_2 = UnitVector::new_unchecked(normal.cross(&tangent_1));
    [tangent_1, tangent_2]
}

/// Given a normal vector, generate two orthogonal tangent vectors.
pub fn make_random_tangent_ortho_basis(
    normal: &UnitVector,
    rng: &impl RandomSample,
) -> [UnitVector; 2] {
    let tangent_1: UnitVector = {
        let mut non_parallel = Vector::new(0., 0., 0.);
        let mut tangent = Vector::new(0., 0., 0.);
        const STEPS: usize = 10;
        for step in 0..STEPS {
            for axis in 0..3 {
                non_parallel[axis] = rng.random_sample(&*NORMAL_RNG_DIST);
            }
            non_parallel /= non_parallel.norm();
            tangent = non_parallel.cross(normal);
            if tangent.norm() > EPSILON {
                break;
            } else if step == STEPS - 1 {
                panic!(
                    "Failed to construct orthonormal basis: last norm was {}, last 
                    non_parallel was {:?}, last tangent {:?}, normal {:?}",
                    tangent.norm(),
                    non_parallel,
                    tangent,
                    normal
                );
            }
        }
        UnitVector::new_normalize(tangent)
    };
    let tangent_2 = UnitVector::new_unchecked(normal.cross(&tangent_1));
    [tangent_1, tangent_2]
}

fn reflect(normal: &UnitVector, incident: &UnitVector) -> UnitVector {
    UnitVector::new_unchecked(
        incident.into_inner() - 2. * normal.into_inner() * normal.dot(incident),
    )
}

lazy_static! {
    /// Static normal random number distribution.
    pub static ref NORMAL_RNG_DIST: rand_distr::Normal<Float> =
        rand_distr::Normal::<Float>::new(0., 1.).unwrap();
}

/// Wrapper around a random number generator for computing samples.
pub trait RandomSample: Sized + Default {
    /// Produce a random float in the interval [0, 1] for generating random
    /// samples.
    fn random_float(&self) -> Float;

    /// Produce a random float according to the supplied probability
    /// distribution.
    fn random_sample<D: rand::distributions::Distribution<Float>>(&self, dist: &D) -> Float;
}

/// A mock RNG that always returns the same value for performance testing and
/// tests.
#[derive(Clone, Default)]
pub struct MockRandomSampleEngine {}

impl RandomSample for MockRandomSampleEngine {
    fn random_float(&self) -> Float {
        1.
    }
    fn random_sample<D: rand::distributions::Distribution<Float>>(&self, _dist: &D) -> Float {
        1.
    }
}

/// A deterministically seeded random number generator for testing.
#[derive(Clone, Debug)]
pub struct DeterministicRandomEngine {
    /// The random number generator.
    ///
    /// Uses a `RefCell` wrapper to allow interior mutability.
    rng: std::cell::RefCell<rand::rngs::StdRng>,
}

impl Default for DeterministicRandomEngine {
    fn default() -> Self {
        use rand::SeedableRng;
        let rng = rand::rngs::StdRng::seed_from_u64(1);
        let rng = std::cell::RefCell::new(rng);
        Self { rng }
    }
}

impl RandomSample for DeterministicRandomEngine {
    fn random_float(&self) -> Float {
        use rand::Rng;
        self.rng.borrow_mut().gen()
    }
    fn random_sample<D: rand::distributions::Distribution<Float>>(&self, dist: &D) -> Float {
        use rand::Rng;
        self.rng.borrow_mut().sample(dist)
    }
}

#[derive(Clone)]
pub struct RandomSampleEngine {}

impl Default for RandomSampleEngine {
    fn default() -> Self {
        Self {}
    }
}

impl RandomSample for RandomSampleEngine {
    fn random_float(&self) -> Float {
        use rand::Rng;
        rand::thread_rng().gen()
    }
    fn random_sample<D: rand::distributions::Distribution<Float>>(&self, dist: &D) -> Float {
        use rand::Rng;
        rand::thread_rng().sample(dist)
    }
}

/// Container for progressively updated mean and variance estimates for a stream
/// of numbers.
///
/// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
#[derive(Debug)]
pub struct StatsStream {
    /// The mean.
    x_bar: Float,
    /// Parameter related to the standard deviation.
    m: Float,
    /// Number of samples seen so far.
    n: usize,
}

impl Default for StatsStream {
    fn default() -> Self {
        Self {
            x_bar: 0.,
            m: 0.,
            n: 0,
        }
    }
}

impl StatsStream {
    /// Retrieve the mean.
    pub fn mean(&self) -> Float {
        self.x_bar
    }
    /// Retrieve the standard deviation.
    pub fn standard_dev(&self) -> Float {
        (self.m / (-1. + self.n as Float)).sqrt()
    }
    /// Retrieve the standard deviation of the mean.
    pub fn standard_error(&self) -> Float {
        self.standard_dev() / (self.n as Float).sqrt()
    }
    /// Retrieve the number of samples seen.
    pub fn count(&self) -> usize {
        self.n
    }

    /// Add a new element.
    pub fn append(&mut self, x: Float) {
        self.n += 1;
        let prev_x_bar = self.x_bar;
        self.x_bar = prev_x_bar + (x - prev_x_bar) / (self.n as Float);
        self.m += (x - prev_x_bar) * (x - self.x_bar);
    }
    /// Add several new elements.
    pub fn extend(&mut self, vals: impl IntoIterator<Item = Float>) {
        for val in vals {
            self.append(val);
        }
    }
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    use super::*;

    #[test]
    fn test_delta_factor() {
        let delta_factor_1 = DeltaFactor::from(7.);
        let delta_factor_2 = DeltaFactor::from(2.).with_num_deltas(1);
        let delta_factor_3 = (delta_factor_1 + 2. * delta_factor_2.powi(2)) / 2.;

        assert_relative_eq!(delta_factor_3.factor(), 4.);
        assert_eq!(delta_factor_3.num_deltas(), 2);

        let delta_factor_quotient_1 = delta_factor_3 / delta_factor_1;
        let delta_factor_quotient_2 = delta_factor_3 / delta_factor_3;

        assert_eq!(delta_factor_quotient_1.num_deltas(), 2);
        assert_eq!(delta_factor_quotient_2.num_deltas(), 0);
        assert_relative_eq!(delta_factor_quotient_1.factor(), 4. / 7.);
        assert_relative_eq!(delta_factor_quotient_2.factor(), 1.);
    }

    #[test]
    fn test_importance_sample() {
        let sample_1 = ImportanceSample::new(2., 1.);
        let sample_2 = ImportanceSample::new(4., 10.).with_num_deltas(1);
        let sample_3 = ImportanceSample::zero();
        let sample_4 = ImportanceSample::unity();

        assert_relative_eq!(2., sample_1.quotient());
        assert_relative_eq!(0.4, sample_2.quotient());

        assert_relative_eq!(1., (sample_2 / 4.).value().factor());
        assert_relative_eq!(10., (sample_2 / 4.).prob_density().factor());
        assert_relative_eq!(0.1, (sample_2 / 4.).quotient());

        assert_relative_eq!(8., (sample_1 * sample_2).value().factor());
        assert_relative_eq!(10., (sample_1 * sample_2).prob_density().factor());
        assert_relative_eq!(0.8, (sample_1 * sample_2).quotient());

        assert_relative_eq!(0., (sample_1 * sample_3).value().factor());
        assert_relative_eq!(1., (sample_1 * sample_3).prob_density().factor());
        assert_relative_eq!(0., (sample_1 * sample_3).quotient());

        assert_relative_eq!(
            sample_1.value().factor(),
            (sample_1 * sample_4).value().factor()
        );
        assert_relative_eq!(
            sample_1.prob_density().factor(),
            (sample_1 * sample_4).prob_density().factor()
        );
        assert_relative_eq!(sample_1.quotient(), (sample_1 * sample_4).quotient());
    }

    #[test]
    fn test_reflect() {
        let normal = Vector::x_axis();
        let incident = UnitVector::new_normalize(-x_hat() + y_hat());
        let reflected = super::reflect(&normal, &incident);
        assert_eq!(reflected, UnitVector::new_normalize(x_hat() + y_hat()));
    }

    #[test]
    fn test_refract() {
        let rng = DeterministicRandomEngine::default();
        let index_of_refraction: Float = 1.5;
        let refractive_bsdf = RefractiveBSDF::new(index_of_refraction);

        // Refracted from lower index of refraction material to higher.
        {
            let incident = UnitVector::new_normalize(-x_hat() + 0.1 * y_hat());
            let normal = OrientedNormal {
                dir: Vector::x_axis(),
                is_outward: true,
            };
            let (refracted, _) = refractive_bsdf.importance_sample(&normal, &incident, true, &rng);
            let sin_incident_normal = (1. - normal.dot(&incident).powf(2.)).sqrt();
            let sin_refracted_normal = (1. - normal.dot(&refracted).powf(2.)).sqrt();
            assert_eq!(
                normal.dot(&incident).signum(),
                normal.dot(&refracted).signum()
            );
            assert_relative_eq!(
                sin_incident_normal,
                index_of_refraction * sin_refracted_normal,
                epsilon = 1e-5
            );
        }
        // Refracted from higher index of refraction material to lower.
        {
            let incident = UnitVector::new_normalize(-x_hat() + 0.1 * y_hat());
            let normal = OrientedNormal {
                dir: -Vector::x_axis(),
                is_outward: true,
            };
            let (refracted, _) = refractive_bsdf.importance_sample(&normal, &incident, true, &rng);
            let sin_incident_normal = (1. - normal.dot(&incident).powf(2.)).sqrt();
            let sin_refracted_normal = (1. - normal.dot(&refracted).powf(2.)).sqrt();
            assert_eq!(
                normal.dot(&incident).signum(),
                normal.dot(&refracted).signum()
            );
            assert_relative_eq!(
                index_of_refraction * sin_incident_normal,
                sin_refracted_normal,
                epsilon = 1e-5
            );
        }
        // Total internal reflection.
        {
            let incident = UnitVector::new_normalize(-x_hat() + y_hat());
            let normal = OrientedNormal {
                dir: -Vector::x_axis(),
                is_outward: true,
            };
            let (reflected, _) = refractive_bsdf.importance_sample(&normal, &incident, true, &rng);
            assert_relative_eq!(
                -normal.dot(&incident),
                normal.dot(&reflected),
                epsilon = 1e-5
            );
        }
    }

    #[test]
    fn test_bsdfs() {
        let glossy = GlossyBSDF::new((30f64).to_radians());
        let matte = MatteBSDF::default();
        let specular = SpecularBSDF::default();
        let refractive = RefractiveBSDF::new(1.3);

        // Matte, specular, and refractive.
        check_bsdf_norm_imp_sampled(&matte, 1.);
        check_bsdf_norm_hemisph_sampled(&matte, 1.);
        check_bsdf_norm_imp_sampled(&specular, 1.);
        check_bsdf_norm_imp_sampled(&refractive, 1.);

        // Glossy.
        check_bsdf_norm_imp_sampled_head_on(&GlossyBSDF::new((90f64).to_radians()), 1.);
        check_bsdf_norm_imp_sampled_head_on(&glossy, 1.);
        check_bsdf_norm_hemisph_sampled(&glossy, 1.);
        // Regression test. 0.506 isn't derived from anything.
        check_bsdf_norm_imp_sampled_oblique(&glossy, 0.506);

        // Scaled.
        let scaled_matte = ScaledBSDF::new(matte.clone(), 0.5);
        check_bsdf_norm_imp_sampled(&scaled_matte, 0.5);
        check_bsdf_norm_hemisph_sampled(&scaled_matte, 0.5);
        check_bsdf_norm_imp_sampled(&ScaledBSDF::new(specular.clone(), 0.219), 0.219);

        // Combined.
        check_bsdf_norm_imp_sampled(&specular.clone().combine_with(0.2, matte.clone(), 0.8), 1.);
        let glossy_combined_with_matte = glossy.clone().combine_with(0.3, matte.clone(), 0.7);
        check_bsdf_norm_imp_sampled_head_on(&glossy_combined_with_matte, 1.);
        check_bsdf_norm_hemisph_sampled(&glossy_combined_with_matte, 1.);

        // Scaled & combined.
        check_bsdf_norm_imp_sampled(
            &specular.clone().combine_with(0.111, matte.clone(), 0.111),
            0.222,
        );

        // Tests for `eval_sampling_prob_density`.
        {
            let rng = DeterministicRandomEngine::default();
            check_bsdf_eval_sampling_prob_density(&glossy, &rng);
            check_bsdf_eval_sampling_prob_density(&matte, &rng);
            check_bsdf_eval_sampling_prob_density(&scaled_matte, &rng);
            check_bsdf_eval_sampling_prob_density(&glossy_combined_with_matte, &rng);
        }
    }
    fn check_bsdf_eval_sampling_prob_density<B: SamplingBSDF>(
        bsdf: &B,
        rng: &DeterministicRandomEngine,
    ) {
        let normal = OrientedNormal {
            dir: Vector::x_axis(),
            is_outward: true,
        };
        for y_comp_weight in [1e-3, 0.1, -0.5, 2., -100.] {
            let incident = UnitVector::new_normalize(x_hat() + y_comp_weight * y_hat());
            for _ in 0..100 {
                let (exitant, importance_sample) =
                    bsdf.importance_sample(&normal, &incident, true, rng);
                let sampling_prob_density =
                    bsdf.eval_sampling_prob_density(&normal, &incident, &exitant);
                assert_relative_eq!(
                    importance_sample.prob_density().factor(),
                    sampling_prob_density.factor(),
                    epsilon = 1e-7
                )
            }
        }
    }
    fn check_bsdf_norm_imp_sampled<B: SamplingBSDF>(bsdf: &B, expected_normalization: Float) {
        check_bsdf_norm_imp_sampled_head_on(bsdf, expected_normalization);
        check_bsdf_norm_imp_sampled_oblique(bsdf, expected_normalization);
    }
    fn check_bsdf_norm_imp_sampled_head_on<B: SamplingBSDF>(
        bsdf: &B,
        expected_normalization: Float,
    ) {
        check_bsdf_norm_imp_sampled_impl(bsdf, expected_normalization, &(0. * x_hat()));
    }
    fn check_bsdf_norm_imp_sampled_oblique<B: SamplingBSDF>(
        bsdf: &B,
        expected_normalization: Float,
    ) {
        check_bsdf_norm_imp_sampled_impl(bsdf, expected_normalization, &(2. * x_hat()));
    }
    /// Check that a BSDF's normalization is as expected using importance
    /// sampling.
    fn check_bsdf_norm_imp_sampled_impl<B: SamplingBSDF>(
        bsdf: &B,
        expected_normalization: Float,
        perp_component: &Vector,
    ) {
        let rng = DeterministicRandomEngine::default();

        let normal = OrientedNormal {
            dir: Vector::z_axis(),
            is_outward: true,
        };
        let incident = -UnitVector::new_normalize(normal.dir.into_inner() + perp_component);

        let mut sum: Float = 0.;
        let num_samples: usize = 10000;
        for _ in 0..num_samples {
            let (sampled_exitant, function_sample) =
                bsdf.importance_sample(&normal, &incident, true, &rng);
            sum += function_sample.quotient();
            assert_relative_eq!(sampled_exitant.norm(), 1.);
        }
        let normalization = sum / num_samples as Float;
        assert_relative_eq!(expected_normalization, normalization, epsilon = 0.01);
    }
    /// Check that a BSDF's normalization is as expected using uniform
    /// hemispheric sampling.
    fn check_bsdf_norm_hemisph_sampled<B: SamplingBSDF>(bsdf: &B, expected_normalization: Float) {
        let rng = DeterministicRandomEngine::default();

        let normal = OrientedNormal {
            dir: Vector::z_axis(),
            is_outward: true,
        };
        let incident = -normal.dir;

        let mut sum: Float = 0.;
        let num_samples: usize = 10000;
        for _ in 0..num_samples {
            let sampled_exitant = super::sample_from_hemisphere(&normal.dir, &rng);
            let function_sample = bsdf.eval(&normal, &incident, &sampled_exitant);
            sum += function_sample.unwrap_value() * (2. * PI) * normal.dot(&sampled_exitant);
        }
        let normalization = sum / num_samples as Float;
        assert_relative_eq!(expected_normalization, normalization, epsilon = 0.03);
    }

    #[test]
    fn test_sample_from_hemisphere() {
        let rng = DeterministicRandomEngine::default();
        let num_samples = 10000;

        // Generate `num_samples` hemispheric samples. Ensure each one is as expected.
        let total: Vector = (0..num_samples)
            .map(|_| super::sample_from_hemisphere(&Vector::y_axis(), &rng).into_inner())
            .inspect(|sample: &Vector| {
                assert!(sample[1] >= 0.);
                for axis in 0..2 {
                    assert!(sample[axis].abs() < 1.);
                }
                assert_relative_eq!(sample.norm(), 1., epsilon = EPSILON);
            })
            .sum();
        let mean = total / Float::from(num_samples);

        // Ensure the tangent components average out to 0.
        assert_relative_eq!(mean[0], 0., epsilon = 1e-2);
        assert_relative_eq!(mean[2], 0., epsilon = 1e-2);
        // Ensure the normal component averages to $\int_0^1 \mu d\mu / 4\pi = 1/2$.
        assert_relative_eq!(mean[1], 0.5, epsilon = 1e-2);
    }

    #[test]
    fn test_stats_stream() {
        let iter = (0..10).map(Float::from);
        let mut stats_stream = StatsStream::default();
        stats_stream.extend(iter);
        assert_relative_eq!(4.5, stats_stream.mean());
        assert_relative_eq!((55_f64 / 6.).sqrt(), stats_stream.standard_dev());
    }

    #[test]
    fn rngtest_() {
        rng_test_impl(RandomSampleEngine::default());
        rng_test_impl(DeterministicRandomEngine::default());
    }

    fn rng_test_impl<R: RandomSample>(rng: R) {
        for _ in 0..100 {
            let _ = rng.random_float();
            // Ensure we can call twice.
            let rand = rng.random_float();
            assert!(0. < rand);
            assert!(rand < 1.);
        }
    }

    #[test]
    fn test_emit_profile() {
        let profile = EmitProfile::new([(0, 0.123), (3, 1.1)]);
        let rng = DeterministicRandomEngine::default();
        assert_relative_eq!(0.123 + 1.1 * (0.5f64).powf(3.), profile.eval(0.5));
        assert_relative_eq!(
            (0..10000)
                .map(|_| {
                    let (_, importance_sample) = profile.importance_sample(&Vector::x_axis(), &rng);
                    importance_sample.quotient()
                })
                .sum::<Float>()
                / 10000.,
            profile.integral(),
            epsilon = 2e-2
        );
    }

    #[test]
    fn test_generate_tangent_ortho_basis() {
        let rng = DeterministicRandomEngine::default();
        let normal = UnitVector::new_normalize(Vector::new(1., -2., 5.));
        let [tangent_1, tangent_2] = super::make_random_tangent_ortho_basis(&normal, &rng);
        assert_relative_eq!(normal.dot(&tangent_1), 0., epsilon = 1e-8);
        assert_relative_eq!(normal.dot(&tangent_2), 0., epsilon = 1e-8);
        assert_relative_eq!(tangent_1.dot(&tangent_2), 0., epsilon = 1e-8);
    }
}
