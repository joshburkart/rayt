use crossbeam;
use image as img;
use indicatif;
use num_cpus;

use crate::{math, scene as sc, trace, Float};

pub type Pixel = img::Luma<u8>;
pub type Image = img::ImageBuffer<Pixel, Vec<<Pixel as img::Pixel>::Subpixel>>;

pub struct Renderable {
    /// The `Scene` to render.
    pub scene: sc::Scene,
    /// Parameters specifying the viewpoint and field of view.
    pub frame: Frame,
}

pub struct Renderer<'a, R: math::RandomSample + Send + Clone + 'a> {
    ray_tracer: &'a trace::RayTracerImpl<R>,
    num_threads: Option<usize>,
}

impl<'a, R: math::RandomSample + Send + Clone + 'a> Renderer<'a, R> {
    pub fn new(ray_tracer: &'a trace::RayTracerImpl<R>) -> Self {
        Self {
            ray_tracer,
            num_threads: None,
        }
    }
    pub fn with_num_threads(self, num_threads: usize) -> Self {
        Self {
            num_threads: Some(num_threads),
            ..self
        }
    }

    pub fn render(&self, renderable: Renderable, height_pixels: usize) -> Image {
        self.render_impl(renderable, height_pixels, None::<&fn(usize)>)
    }
    pub fn render_with_progress(&self, renderable: Renderable, height_pixels: usize) -> Image {
        static PROGRESS_INC: usize = 100;
        static PROGRESS_INC_U64: u64 = PROGRESS_INC as u64;
        let progress_bar =
            indicatif::ProgressBar::new(renderable.frame.num_rays(height_pixels) as u64);
        progress_bar.set_style(
            indicatif::ProgressStyle::default_bar()
                .template(
                    "{pos}/{len} {percent:>3}% {wide_bar:.white/blue} \
                     {elapsed} elapsed, {eta} remain",
                )
                .progress_chars("##-"),
        );
        let image = self.render_impl(
            renderable,
            height_pixels,
            Some(&|flat_index| {
                if flat_index % PROGRESS_INC == 0 {
                    progress_bar.inc(PROGRESS_INC_U64)
                }
            }),
        );
        progress_bar.finish();
        image
    }
    fn render_impl<F: Fn(usize)>(
        &self,
        renderable: Renderable,
        height_pixels: usize,
        maybe_callback: Option<&F>,
    ) -> Image
    where
        F: Send + Sync,
    {
        let frame = renderable.frame.clone();
        let scene = &renderable.scene;

        // Ray tracing results get collected into this output buffer before being
        // constituted into an image.
        let output_data_buffer = std::sync::Mutex::new(Vec::<((u32, u32), Float)>::new());

        // Use this mechanism of spawning threads so that we can borrow local variables.
        crossbeam::scope(|scope| {
            // Channel to send pixels for workers to ray trace.
            let (input_sender, input_receiver) = crossbeam::channel::bounded(0);
            // Channel for workers to output completed ray trace results.
            let (output_sender, output_receiver) = crossbeam::channel::bounded(0);

            // Thread to send rays to workers.
            let sender_handle = scope.spawn(move |_| {
                for message in frame.iter_rays(height_pixels) {
                    input_sender.send(message).unwrap();
                }
                // Disconnect input channel now that we've sent everything along.
                drop(input_sender);
            });

            // Ray tracer worker threads.
            let mut worker_handles = vec![];
            let num_threads = self.num_threads.unwrap_or_else(num_cpus::get);
            for _ in 0..num_threads {
                // Clone various items that workers need.
                let worker_tracer = (*self.ray_tracer).clone();
                let input_receiver = input_receiver.clone();
                let output_sender = output_sender.clone();

                // Make worker thread.
                worker_handles.push(scope.spawn(move |_| {
                    // Run ray tracing for each work item sent along (i.e. each pixel) and pass
                    // result back to main thread.
                    input_receiver.iter().for_each(|((x_index, y_index), ray)| {
                        let result = (
                            (x_index as u32, y_index as u32),
                            worker_tracer.trace(scene, ray),
                        );

                        output_sender.send(result).unwrap();
                    });
                }));
            }

            // Thread to receive ray tracing outputs and collect into a vector.
            let output_handle = {
                let output_data_buffer = &output_data_buffer;
                scope.spawn(move |_| {
                    let receive_iter = output_receiver.iter();
                    *output_data_buffer.lock().unwrap() = if let Some(callback) = maybe_callback {
                        receive_iter
                            .enumerate()
                            .map(|(index, work_item)| {
                                callback(index);
                                work_item
                            })
                            .collect::<Vec<_>>()
                    } else {
                        receive_iter.collect::<Vec<_>>()
                    };
                })
            };

            // All work should have been sent along. We can now join the worker threads.
            for worker_handle in worker_handles {
                worker_handle.join().unwrap();
            }
            // Disconnect output channel now that we've processed all work.
            drop(output_sender);

            sender_handle.join().unwrap();
            output_handle.join().unwrap();
        })
        .unwrap();

        // Unpack all the data we generated and make an image.
        let mut output_data_buffer = output_data_buffer.into_inner().unwrap();
        output_data_buffer.sort_by(|a, b| a.1.partial_cmp(&b.1).expect("NaN encountered"));
        println!(
            "\n\nLargest pixels (before applying exposure/contrast): {:?}",
            output_data_buffer.as_slice()[output_data_buffer.len() - 6..].to_vec()
        );
        println!(
            "Mean pixel brightness (before applying exposure/contrast): {:.4}\n\n",
            output_data_buffer.iter().map(|(_, val)| val).sum::<Float>()
                / (output_data_buffer.len() as Float),
        );
        self.make_image(
            renderable.frame.dimensions(height_pixels),
            renderable.scene.exposure,
            renderable.scene.contrast_level,
            output_data_buffer,
        )
    }

    /// Constitute an output data buffer into an image.
    fn make_image(
        &self,
        dimensions: (usize, usize),
        exposure: Float,
        contrast_level: i8,
        data_buffer: std::vec::Vec<((u32, u32), Float)>,
    ) -> img::GrayImage {
        let mut image = img::GrayImage::new(dimensions.0 as u32, dimensions.1 as u32);
        for ((x_index, y_index), intensity) in data_buffer.iter() {
            let pixel_intensity =
                apply_contrast_level((intensity * exposure).min(1.), contrast_level);
            *image.get_pixel_mut(*x_index, *y_index) =
                img::Luma::<u8>([(pixel_intensity * Float::from(std::u8::MAX)) as u8]);
        }
        image
    }
}

fn apply_contrast_level(pixel_intensity: Float, contrast_level: i8) -> Float {
    fn add_contrast(x: Float) -> Float {
        (1. + (math::PI * (x - 0.5)).sin()) / 2.
    }
    let mut output = pixel_intensity;
    for _ in 0..contrast_level {
        output = add_contrast(output);
    }
    output
}

/// A more convenient representation from which to construct a `Frame`.
pub struct FrameBuilder {
    /// Point where rays are cast from.
    pub observer: math::Point,

    /// Center point for the image. Distance from `observer` is actually
    /// irrelevant, so long as `width`/`height_from_center` are adjusted
    /// accordingly.
    pub center: math::Point,
    pub vertical: math::Vector,
    /// Height of image in scene coordinates, as measured by a chord passing
    /// through `center`.
    pub height: Float,
    /// Ratio of width to height.
    pub aspect_ratio: Float,
}

impl FrameBuilder {
    /// Build the `Frame`.
    pub fn build(self) -> Frame {
        let height = self.height;
        let width = height * self.aspect_ratio;

        let r_hat = (self.center - self.observer).normalize();

        // Remove any radial component from `vertical`.
        let vertical = self.vertical - r_hat * self.vertical.dot(&r_hat);

        let y_vec = -height * vertical.normalize();
        let x_vec = -width * r_hat.cross(&y_vec).normalize();
        let aspect_ratio = x_vec.norm() / y_vec.norm();

        let ray_base_dir = self.center - self.observer - x_vec / 2. - y_vec / 2.;

        Frame {
            observer: self.observer,
            aspect_ratio,
            ray_base_dir,
            axis_vecs: (x_vec, y_vec),
        }
    }
}

#[derive(Clone)]
pub struct Frame {
    /// The location from which rays will be cast.
    pub observer: math::Point,

    /// The aspect ratio of the image.
    aspect_ratio: Float,

    axis_vecs: (math::Vector, math::Vector),
    ray_base_dir: math::Vector,
}

impl Frame {
    /// Get the dimensions of the frame.
    pub fn dimensions(&self, height_pixels: usize) -> (usize, usize) {
        (
            (self.aspect_ratio * height_pixels as Float) as usize,
            height_pixels,
        )
    }
    /// Get the number of rays required to render.
    pub fn num_rays(&self, height_pixels: usize) -> usize {
        let dimensions = self.dimensions(height_pixels);
        dimensions.0 * dimensions.1
    }

    /// Translate image coordinates into a `Ray`.
    pub fn image_coords_to_ray(
        &self,
        dimensions: (usize, usize),
        image_coords: (usize, usize),
    ) -> math::Ray {
        let dx = (image_coords.0 as Float) / (dimensions.0 as Float - 1.);
        let dy = (image_coords.1 as Float) / (dimensions.1 as Float - 1.);

        math::Ray::new(
            self.observer,
            math::UnitVector::new_normalize(
                self.ray_base_dir + dx * self.axis_vecs.0 + dy * self.axis_vecs.1,
            ),
        )
    }

    /// Iterate over `Ray`s.
    pub fn iter_rays(
        self,
        height_pixels: usize,
    ) -> impl Iterator<Item = ((usize, usize), math::Ray)> {
        use itertools::Itertools;

        let dimensions = self.dimensions(height_pixels);
        (0..dimensions.0)
            .cartesian_product(0..dimensions.1)
            .map(move |image_coords| {
                (
                    image_coords,
                    self.image_coords_to_ray(dimensions, image_coords),
                )
            })
    }
}

#[cfg(test)]
mod tests {
    use nalgebra as na;

    use crate::{math, scene as sc, trace};

    use super::*;

    const BOX_HALF_WIDTH: Float = 4.;
    const REFLECT_WEIGHT: Float = 0.7;

    #[test]
    fn test_no_crash() {
        let ray_tracer = trace::RayTracer::default().with_eye_trace_depth(Some(4));
        let renderer = Renderer::new(&ray_tracer).with_num_threads(1);
        let renderable = {
            let frame = FrameBuilder {
                observer: math::Point::new(-2., -2., -1.),

                center: math::Point::new(0.5, 0.5, 0.),
                vertical: math::z_hat(),
                aspect_ratio: 16. / 9.,
                height: 3.,
            }
            .build();
            let scene = make_scene();
            Renderable { scene, frame }
        };
        let _image = renderer.render(renderable, 10);
    }

    fn make_scene() -> sc::Scene {
        let mut objects = vec![
            // Emitting balls.
            sc::Object {
                shape: sc::Shape::make_ball(0.5),
                isometry: na::Isometry3::translation(0., 2., 1.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                    math::EmitProfile::identity(),
                )),
            },
            sc::Object {
                shape: sc::Shape::make_ball(0.5),
                isometry: na::Isometry3::translation(2., 0., 0.),
                material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                    math::EmitProfile::identity(),
                )),
            },
            // Absorbing ball.
            sc::Object {
                shape: sc::Shape::make_ball(1.),
                isometry: na::Isometry3::translation(3., 3., -1.),
                material: sc::Material::absorbing(),
            },
            // Infinity absorbing ball.
            sc::Object {
                shape: sc::Shape::make_ball(1000.),
                isometry: na::Isometry3::identity(),
                material: sc::Material::absorbing(),
            },
        ];

        // A cube of reflective planar walls, each placed at ±`BOX_HALF_WIDTH`.
        objects.push(sc::Object {
            shape: sc::Shape::make_cuboid(math::Vector::new(
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
            )),
            isometry: math::Isometry::identity(),
            material: sc::Material::with_surface_params(sc::SurfaceParams::Scattering(
                sc::ScatteringParams::specular(REFLECT_WEIGHT),
            )),
        });
        objects.push(sc::Object {
            shape: sc::Shape::make_cuboid(math::Vector::new(
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
                BOX_HALF_WIDTH,
            )),
            isometry: math::Isometry::identity(),
            material: sc::Material::with_surface_params(sc::SurfaceParams::Emitting(
                math::EmitProfile::identity() * 0.4,
            )),
        });

        sc::Scene::from_objects(objects)
    }

    #[test]
    fn test_apply_contrast_level() {
        for contrast_level in 0..5 {
            approx::assert_relative_eq!(apply_contrast_level(0., contrast_level), 0.);
            approx::assert_relative_eq!(apply_contrast_level(0.5, contrast_level), 0.5);
            approx::assert_relative_eq!(apply_contrast_level(1., contrast_level), 1.);
        }
        for contrast_level in 1..5 {
            assert!(apply_contrast_level(0.2, contrast_level) < 0.2);
            assert!(apply_contrast_level(0.6, contrast_level) > 0.6);
        }
    }
}
