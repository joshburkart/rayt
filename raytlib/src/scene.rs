use std::sync;

use nalgebra as na;
use ncollide3d as nc;

use crate::{
    math::{self, EmitProfile, PI},
    Float,
};

const EPSILON: Float = 1e-7;
const MAX_TOI: Float = 10000.;

/// Information encoding all the objects and light sources in a scene.
pub struct Scene {
    /// All the objects in a scene.
    pub objects: sync::Arc<Vec<Object>>,
    /// Scale intensities by this factor.
    pub exposure: Float,
    /// Add more contrast based on this parameter.
    pub contrast_level: i8,

    /// Data structure to allow sampling from emitting objects in a scene.
    /// Specifies partial sums of object total luminosities paired with
    /// indices into the `objects` field.
    cumul_lumin_and_object_index: sync::Arc<Vec<(Float, usize)>>,
    /// A bounding volume tree for the scene.
    bvt: BoundingVolumeTree,
}

impl Scene {
    /// Create a `Scene` from a vector of `Object`s.
    pub fn from_objects(objects: Vec<Object>) -> Self {
        let objects = sync::Arc::new(objects);
        let bvt = BoundingVolumeTree::new(objects.clone());

        let cumul_lumin_and_object_index = objects
            .iter()
            .enumerate()
            .filter_map(|(index, object)| {
                let luminosity = object.luminosity();
                if luminosity > 0. {
                    Some((index, luminosity))
                } else {
                    None
                }
            })
            .scan(0., |lumin_cumul_sum, (index, lumin)| {
                *lumin_cumul_sum += lumin;
                Some((*lumin_cumul_sum, index))
            })
            .collect::<Vec<(Float, usize)>>();
        let cumul_lumin_and_object_index = sync::Arc::new(cumul_lumin_and_object_index);
        Self {
            objects,
            exposure: 1.,
            contrast_level: 0,
            bvt,
            cumul_lumin_and_object_index,
        }
    }

    /// Make a new `Scene` with a modified exposure.
    pub fn with_exposure(self, exposure: Float) -> Self {
        Self { exposure, ..self }
    }
    /// Make a new `Scene` with a modified contrast.
    pub fn with_contrast_level(self, contrast_level: i8) -> Self {
        Self {
            contrast_level,
            ..self
        }
    }

    /// Perform a single ray cast within a `Scene`.
    pub fn ray_cast<'a>(&'a self, ray: &math::Ray) -> Option<CollisionResult<'a>> {
        if self.objects.len() < 10 {
            Self::ray_cast_with_objects(ray, self.objects.iter())
        } else {
            Self::ray_cast_with_objects(
                ray,
                self.bvt
                    // Find all the objects a ray might intersect with.
                    .find_ray_intersections(ray)
                    .into_iter(),
            )
        }
    }

    /// Helper function for `ray_cast`.
    fn ray_cast_with_objects<'a>(
        ray: &math::Ray,
        objects_iter: impl Iterator<Item = &'a Object>,
    ) -> Option<CollisionResult<'a>> {
        objects_iter
            // Perform a ray cast with each potentially intersecting object.
            .filter_map(|object: &'a Object| {
                object
                    .ray_cast(ray)
                    .map(|intersection| (intersection, object))
            })
            // Find the closest collision.
            .min_by(|(intersection_1, _), (intersection_2, _)| {
                intersection_1
                    .time_of_impact
                    .partial_cmp(&intersection_2.time_of_impact)
                    .unwrap()
            })
            // Post process to create `CollisionResult`s.
            .map(|(intersection, object)| CollisionResult {
                point: intersection.point,
                normal: intersection.normal,
                object,
            })
    }

    /// Determine whether there is an intervening surface between two points in
    /// the scene.
    pub fn has_direct_line_of_sight(&self, point_1: &math::Point, point_2: &math::Point) -> bool {
        let dir_unnorm = point_2 - point_1;
        let ray = math::Ray {
            origin: *point_1,
            dir: math::UnitVector::new_normalize(dir_unnorm),
        };
        let result = self.ray_cast(&ray);
        match result {
            None => true,
            Some(CollisionResult { point, .. }) => (point_1 - point).norm() >= (dir_unnorm).norm(),
        }
    }

    /// Sample a point from an emissive object from the perspective of an
    /// observer.
    pub fn sample_emitting_point_for_observer<'a, RS: math::RandomSample>(
        &'a self,
        observer: &math::Point,
        rng: &RS,
    ) -> EmitSample<'a> {
        let (object, prob_of_picking_object) = self.sample_emitting_object(rng);

        let (point, normal, area_prob_dens) =
            object
                .shape
                .sample_point_for_observer(observer, &object.isometry, rng);

        let emit_profile =
            if let SurfaceParams::Emitting(power_law_dist) = &object.material.surface_params {
                power_law_dist
            } else {
                panic!("Should have found an emitting object");
            };

        let area_prob_dens = area_prob_dens * prob_of_picking_object;

        EmitSample {
            point,
            normal: math::OrientedNormal {
                dir: normal,
                is_outward: true,
            },
            emit_profile,
            area_prob_dens,
        }
    }
    /// Sample a point from an emissive object independent of observer.
    pub fn sample_emitting_point<'a, RS: math::RandomSample>(&'a self, rng: &RS) -> EmitSample<'a> {
        let (object, prob_of_picking_object) = self.sample_emitting_object(rng);

        // Pick a random point.
        let (point, normal): (math::Point, math::UnitVector) =
            object.shape.sample_point(&object.isometry, rng);

        let emit_profile =
            if let SurfaceParams::Emitting(power_law_dist) = &object.material.surface_params {
                power_law_dist
            } else {
                panic!("Should have found an emitting object");
            };

        let area_prob_dens = prob_of_picking_object / object.shape.surface_area();

        EmitSample {
            point,
            normal: math::OrientedNormal {
                dir: normal,
                is_outward: true,
            },
            emit_profile,
            area_prob_dens,
        }
    }
    pub fn prob_of_picking_emitting_object(&self, object: &Object) -> Float {
        object.luminosity() / self.total_luminosity()
    }
    pub fn compute_area_prob_density(&self, object: &Object) -> Float {
        1. / object.shape.surface_area()
    }
    fn total_luminosity(&self) -> Float {
        self.cumul_lumin_and_object_index
            .last()
            .expect("No emitting objects?")
            .0
    }
    /// Select an object randomly, weighted by luminosity (integrated radiance).
    pub fn sample_emitting_object<RS: math::RandomSample>(&self, rng: &RS) -> (&Object, Float) {
        let total_luminosity = self.total_luminosity();
        let rand = rng.random_float() * total_luminosity;
        let index_into_cumul =
            match self
                .cumul_lumin_and_object_index
                .binary_search_by(|(cumul_lumin, _)| {
                    cumul_lumin
                        .partial_cmp(&rand)
                        .expect("Couldn't compare values")
                }) {
                Ok(index) => index,
                Err(index) => index,
            };
        if index_into_cumul == self.cumul_lumin_and_object_index.len() {
            panic!(
                "No emitting objects? {:?}",
                self.cumul_lumin_and_object_index
            );
        }
        let object = &self.objects[self.cumul_lumin_and_object_index[index_into_cumul].1];
        (object, self.prob_of_picking_emitting_object(object))
    }
}

/// Info related to a sampled emitted "light ray".
#[derive(Clone, Debug)]
pub struct EmitSample<'a> {
    pub point: math::Point,
    pub normal: math::OrientedNormal,
    // The associated probability density is per object luminosity and per surface area.
    pub area_prob_dens: Float,
    pub emit_profile: &'a math::EmitProfile,
}

/// An object.
#[derive(Clone, Debug)]
pub struct Object {
    /// The object's shape.
    pub shape: Shape,
    /// The material the shape is made of.
    pub material: Material,
    /// An isometry that is applied to the shape.
    pub isometry: math::Isometry,
}

impl Object {
    /// Get the object's axis-aligned bounding box.
    pub fn axis_aligned_bounding_box(&self) -> AxisAlignedBoundingBox {
        self.shape.axis_aligned_bounding_box(&self.isometry)
    }

    pub fn ray_cast(&self, ray: &math::Ray) -> Option<RayIntersection> {
        let ray_intersection_nc = self.shape.toi_and_normal_with_ray(&self.isometry, ray)?;
        let mut normal = math::UnitVector::new_normalize(ray_intersection_nc.normal);
        if normal.dot(&ray.dir) > 0. {
            // Sometimes bad normals appear to be returned. Account for this.
            normal = -normal;
        }
        let time_of_impact = ray_intersection_nc.toi;
        let point = ray.origin + ray.dir.into_inner() * time_of_impact;
        let is_outward_normal = self.is_outward_normal(&point, &ray_intersection_nc);
        assert!(time_of_impact > -EPSILON);
        Some(RayIntersection {
            time_of_impact,
            point,
            normal: math::OrientedNormal {
                dir: normal,
                is_outward: is_outward_normal,
            },
        })
    }
    fn is_outward_normal(
        &self,
        point: &math::Point,
        intersection: &nc::query::RayIntersection<Float>,
    ) -> bool {
        match &self.shape {
            Shape::TriMesh {
                shape_container, ..
            } => {
                let face = &shape_container.shape.faces()[shape_container
                    .shape
                    .face_containing_feature(intersection.feature)];
                if let Some(normal) = face.normal {
                    normal.dot(&intersection.normal) > 0.
                } else {
                    // Something pathological has happened, so hopefully shouldn't affect the image?
                    false
                }
            }
            _ => {
                let center_to_point = point - self.isometry.transform_point(&math::Point::origin());
                center_to_point.dot(&intersection.normal) > 0.
            }
        }
    }

    /// The total amount of energy emitted from the object per time.
    pub fn luminosity(&self) -> Float {
        if let SurfaceParams::Emitting(power_law_dist) = &self.material.surface_params {
            power_law_dist.integral() * self.shape.surface_area()
        } else {
            0.
        }
    }
}

/// Specification of an object's optical properties.
#[derive(Debug, Clone)]
pub struct Material {
    /// The surface optical properties of an object.
    pub surface_params: SurfaceParams,
}

impl Material {
    pub fn with_surface_params(surface_params: SurfaceParams) -> Self {
        Self { surface_params }
    }
    pub fn absorbing() -> Self {
        Self::with_surface_params(SurfaceParams::Scattering(ScatteringParams::matte(0.)))
    }
}

/// A wrapper for an `nc::shape::Shape` that also tracks other attributes, e.g.
/// surface area.
#[derive(Clone)]
pub struct ShapeContainer<S: Clone> {
    shape: S,
    surface_area: Float,
}

/// A shape.
#[derive(Clone)]
pub enum Shape {
    /// A ball.
    Ball {
        shape_container: ShapeContainer<nc::shape::Ball<Float>>,
    },
    /// A triangular mesh.
    TriMesh {
        shape_container: ShapeContainer<Box<nc::shape::TriMesh<Float>>>,
        /// Cumulative sum of surface areas of each triangle divided by the
        /// total surface area. For use in sampling.
        cumul_facet_surface_area_fracs: Vec<Float>,
    },
    /// A cuboid.
    Cuboid {
        shape_container: ShapeContainer<nc::shape::Cuboid<Float>>,
    },
}

impl std::fmt::Debug for Shape {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Shape::Ball { .. } => "Ball",
            Shape::TriMesh { .. } => "TriMesh",
            Shape::Cuboid { .. } => "Cuboid",
        })
    }
}

impl Shape {
    // Factory methods.
    pub fn make_ball(radius: Float) -> Self {
        Self::Ball {
            shape_container: ShapeContainer {
                shape: nc::shape::Ball::new(radius),
                surface_area: 4. * PI * radius.powf(2.),
            },
        }
    }
    pub fn make_plane(half_extents: &[math::Vector; 2]) -> Self {
        let center = math::Point::new(0., 0., 0.);
        let points = vec![
            center + half_extents[0] + half_extents[1],
            center + half_extents[0] - half_extents[1],
            center - half_extents[0] - half_extents[1],
            center - half_extents[0] + half_extents[1],
        ];
        let index_triplets = vec![
            na::Point3::<usize>::new(2, 1, 0),
            na::Point3::<usize>::new(2, 0, 3),
        ];
        Self::make_tri_mesh(points, index_triplets)
    }
    pub fn make_tri_mesh(points: Vec<math::Point>, index_triplets: Vec<na::Point3<usize>>) -> Self {
        let shape = Box::new(nc::shape::TriMesh::new(points, index_triplets, None));

        let mut surface_area = 0.;
        let mut cumul_facet_surface_areas = Vec::<Float>::with_capacity(shape.points().len());
        for face in shape.faces() {
            let point_1 = shape.points()[face.indices[0]];
            let point_2 = shape.points()[face.indices[1]];
            let point_3 = shape.points()[face.indices[2]];

            let displacement_12 = point_2 - point_1;
            let displacement_13 = point_3 - point_1;

            // No factor of 0.5 since we count both sides of a facet towards its surface
            // area. Thus we also generate light paths inside a closed triangle
            // mesh.
            let facet_surface_area = 0.5 * displacement_12.cross(&displacement_13).norm();
            surface_area += facet_surface_area;
            cumul_facet_surface_areas.push(surface_area);
        }
        let cumul_facet_surface_area_fracs = cumul_facet_surface_areas
            .iter()
            .map(|cumul_facet_surface_area| cumul_facet_surface_area / surface_area)
            .collect::<Vec<_>>();
        Self::TriMesh {
            shape_container: ShapeContainer {
                shape,
                surface_area,
            },
            cumul_facet_surface_area_fracs,
        }
    }
    pub fn make_cuboid(half_extents: math::Vector) -> Self {
        let shape = nc::shape::Cuboid::new(half_extents);
        let half_extents = shape.half_extents;
        let surface_area = [(0, 1), (0, 2), (1, 2)]
            .iter()
            .map(|(axis_1, axis_2)| 2. * half_extents[*axis_1] * half_extents[*axis_2])
            .sum::<Float>();
        Self::Cuboid {
            shape_container: ShapeContainer {
                shape,
                surface_area,
            },
        }
    }
    pub fn make_disc(radius: Float, outward_normal_dir: math::UnitVector) -> Self {
        let (points, index_triplets) = Self::make_cone_impl(radius, outward_normal_dir, 0., 20);
        Self::make_tri_mesh(points, index_triplets)
    }
    pub fn make_cone(
        radius: f64,
        point_dir: math::UnitVector,
        height: Float,
        num_triangles: usize,
    ) -> Self {
        let cone = Self::make_cone_impl(radius, point_dir, height, num_triangles);
        let cap = Self::make_cone_impl(radius, -point_dir, 0., num_triangles);
        Self::combine_tri_meshes(vec![cone, cap])
    }

    fn make_cone_impl(
        radius: f64,
        point_dir: math::UnitVector,
        height: Float,
        num_triangles: usize,
    ) -> (Vec<math::Point>, Vec<na::Point3<usize>>) {
        let [u, v] = math::make_tangent_orth_basis(&point_dir);

        let (u, v) = if u.cross(&v).dot(&point_dir) > 0. {
            (u, v)
        } else {
            (v, u)
        };

        let mut points = Vec::with_capacity(num_triangles + 1);
        let mut index_triplets = Vec::with_capacity(num_triangles);
        points.push(math::Point::origin() + height * point_dir.into_inner());
        for tri_index in 0..num_triangles {
            let theta = (tri_index as Float) * 2. * math::PI / (num_triangles as Float);
            points.push(
                math::Point::origin()
                    + radius * (u.into_inner() * theta.cos() + v.into_inner() * theta.sin()),
            );
            index_triplets.push(na::Point3::new(
                0,
                tri_index + 1,
                (tri_index + 1) % num_triangles + 1,
            ));
        }

        (points, index_triplets)
    }

    pub fn combine_tri_meshes(tri_meshes: Vec<(Vec<math::Point>, Vec<na::Point3<usize>>)>) -> Self {
        let (num_points, num_triangles) = {
            let mut num_points = 0usize;
            let mut num_triangles = 0usize;
            for tri_mesh in tri_meshes.iter() {
                num_points += tri_mesh.0.len();
                num_triangles += tri_mesh.1.len();
            }
            (num_points, num_triangles)
        };

        let mut cumul_num_points = 0usize;
        let mut points = Vec::<math::Point>::with_capacity(num_points);
        let mut index_triplets = Vec::<na::Point3<usize>>::with_capacity(num_triangles);
        for tri_mesh in tri_meshes {
            for point in tri_mesh.0.iter() {
                points.push(*point);
            }
            for index_triplet in tri_mesh.1 {
                let mut new_index_triplet = index_triplet;
                for axis in 0..3 {
                    new_index_triplet[axis] += cumul_num_points;
                }
                index_triplets.push(new_index_triplet);
            }
            cumul_num_points += tri_mesh.0.len();
        }

        Self::make_tri_mesh(points, index_triplets)
    }

    pub fn load_stl(path: &impl AsRef<std::path::Path>, scale_factor: Float) -> Self {
        let stl = stl_io::read_stl(&mut std::fs::OpenOptions::new().read(true).open(path).unwrap())
            .unwrap();
        Self::make_tri_mesh(
            stl.vertices
                .iter()
                .map(|vertex_vec| {
                    math::Point::new(
                        vertex_vec[0] as Float * scale_factor,
                        vertex_vec[1] as Float * scale_factor,
                        vertex_vec[2] as Float * scale_factor,
                    )
                })
                .collect(),
            stl.faces
                .iter()
                .map(|face| {
                    na::Point3::<usize>::new(face.vertices[0], face.vertices[1], face.vertices[2])
                })
                .collect(),
        )
    }

    /// Get the shape's axis-aligned bounding box.
    pub fn axis_aligned_bounding_box(&self, isometry: &math::Isometry) -> AxisAlignedBoundingBox {
        use nc::bounding_volume::bounding_volume::HasBoundingVolume;
        match self {
            Shape::Ball {
                shape_container, ..
            } => shape_container.shape.bounding_volume(isometry),
            Shape::TriMesh {
                shape_container, ..
            } => shape_container.shape.bounding_volume(isometry),
            Shape::Cuboid {
                shape_container, ..
            } => shape_container.shape.bounding_volume(isometry),
        }
    }
    /// Ray cast with the shape, returning the the time of impact and normal
    /// vector.
    pub fn toi_and_normal_with_ray(
        &self,
        isometry: &math::Isometry,
        ray: &math::Ray,
    ) -> Option<nc::query::RayIntersection<Float>> {
        use nc::query::RayCast;
        match self {
            Shape::Ball {
                shape_container, ..
            } => {
                shape_container
                    .shape
                    .toi_and_normal_with_ray(isometry, &ray.into(), MAX_TOI, false)
            }
            Shape::TriMesh {
                shape_container, ..
            } => {
                shape_container
                    .shape
                    .toi_and_normal_with_ray(isometry, &ray.into(), MAX_TOI, false)
            }
            Shape::Cuboid {
                shape_container, ..
            } => {
                shape_container
                    .shape
                    .toi_and_normal_with_ray(isometry, &ray.into(), MAX_TOI, false)
            }
        }
    }
    /// Compute the surface area.
    pub fn surface_area(&self) -> Float {
        match self {
            Shape::Ball {
                shape_container, ..
            } => shape_container.surface_area,
            Shape::TriMesh {
                shape_container, ..
            } => shape_container.surface_area,
            Shape::Cuboid {
                shape_container, ..
            } => shape_container.surface_area,
        }
    }
    /// Sample a point uniformly by surface area. Also returns the surface
    /// normal at the point.
    pub fn sample_point<RS: math::RandomSample>(
        &self,
        isometry: &math::Isometry,
        rng: &RS,
    ) -> (math::Point, math::UnitVector) {
        let (point_pre_isometry, normal_pre_isometry): (math::Point, math::UnitVector) = match self
        {
            Shape::Ball {
                shape_container, ..
            } => {
                let normal = {
                    let normal = math::Vector::new(
                        rng.random_sample(&*math::NORMAL_RNG_DIST),
                        rng.random_sample(&*math::NORMAL_RNG_DIST),
                        rng.random_sample(&*math::NORMAL_RNG_DIST),
                    );
                    math::UnitVector::new_normalize(normal)
                };
                (
                    math::Point::origin() + shape_container.shape.radius * normal.into_inner(),
                    normal,
                )
            }
            Shape::TriMesh {
                shape_container,
                cumul_facet_surface_area_fracs,
            } => {
                let rand = rng.random_float();
                let face_index = match cumul_facet_surface_area_fracs.binary_search_by(|frac| {
                    frac.partial_cmp(&rand).expect("Couldn't compare values")
                }) {
                    Ok(index) => index,
                    Err(index) => index,
                };
                let index_triplet = shape_container.shape.faces()[face_index].indices;
                let point_1 = shape_container.shape.points()[index_triplet[0]];
                let point_2 = shape_container.shape.points()[index_triplet[1]];
                let point_3 = shape_container.shape.points()[index_triplet[2]];

                let rand_1 = rng.random_float();
                let rand_2 = rng.random_float();

                (
                    // https://math.stackexchange.com/questions/18686/uniform-random-point-in-triangle-in-3d
                    (1. - rand_1.sqrt()) * point_1
                        + rand_1.sqrt() * (1. - rand_2) * point_2.coords
                        + rand_2 * rand_1.sqrt() * point_3.coords,
                    math::UnitVector::new_normalize(
                        (point_2 - point_1).cross(&(point_3 - point_2)),
                    ),
                )
            }
            Shape::Cuboid {
                shape_container, ..
            } => {
                const OTHER_AXES_BY_AXIS: [[usize; 2]; 3] = [[1, 2], [2, 0], [0, 1]];
                let half_extents = &shape_container.shape.half_extents;
                let face_areas = {
                    let mut face_areas = Vec::<_>::with_capacity(3);
                    for other_axes in OTHER_AXES_BY_AXIS {
                        face_areas.push(half_extents[other_axes[0]] * half_extents[other_axes[1]]);
                    }
                    face_areas
                };

                let make_point = |axis, side| {
                    let mut point = math::Point::origin();
                    point[axis] = if side == 0 {
                        half_extents[axis]
                    } else {
                        -half_extents[axis]
                    };
                    for other_axis in OTHER_AXES_BY_AXIS[axis] {
                        point[other_axis] =
                            (2. * rng.random_float() - 1.) * half_extents[other_axis];
                    }
                    point
                };
                let make_normal = |axis, side| {
                    let mut normal = math::Vector::zeros();
                    normal[axis] = if side == 0 { 1. } else { -1. };
                    math::UnitVector::new_unchecked(normal)
                };

                let total_area = 2. * (face_areas[0] + face_areas[1] + face_areas[2]);
                let rand = rng.random_float();
                let mut cumul_surface_area = 0.;
                for (axis, face_area) in face_areas.iter().enumerate() {
                    for side in 0..2 {
                        cumul_surface_area += face_area;
                        if cumul_surface_area / total_area > rand {
                            return (make_point(axis, side), make_normal(axis, side));
                        }
                    }
                }
                (make_point(2, 1), make_normal(2, 1))
            }
        };
        let mut point_post_isometry = isometry.transform_point(&point_pre_isometry);
        let normal_post_isometry = math::UnitVector::new_unchecked(
            isometry.transform_vector(&normal_pre_isometry.into_inner()),
        );
        point_post_isometry += EPSILON * normal_post_isometry.into_inner();
        (point_post_isometry, normal_post_isometry)
    }

    pub fn sample_point_for_observer<RS: math::RandomSample>(
        &self,
        observer: &math::Point,
        isometry: &math::Isometry,
        rng: &RS,
    ) -> (math::Point, math::UnitVector, Float) {
        match self {
            // Approach derived in Shirley, Peter, Changyaw Wang, and Kurt Zimmerman. "Monte carlo
            // techniques for direct lighting calculations." ACM Transactions on Graphics (TOG) 15.1
            // (1996): 1-36.
            Shape::Ball { shape_container } => {
                let r = shape_container.shape.radius;
                let center = isometry.transform_point(&math::Point::new(0., 0., 0.));
                let displacement_to_center = center - observer;

                // Construct an orthonormal basis.
                let (w_hat, distance_to_center) =
                    math::UnitVector::new_and_get(displacement_to_center);
                let v_hat = {
                    let mut v_hat = math::Vector::x_axis();
                    for unit_vec in [
                        math::Vector::x_axis(),
                        math::Vector::y_axis(),
                        math::Vector::z_axis(),
                    ] {
                        if (w_hat.dot(&unit_vec).abs() - 1.).abs() > EPSILON {
                            v_hat = math::UnitVector::new_normalize(w_hat.cross(&unit_vec));
                        }
                    }
                    v_hat
                };
                let u_hat = math::UnitVector::new_normalize(v_hat.cross(&w_hat));

                let rand_1 = rng.random_float();
                let rand_2 = rng.random_float();

                let theta =
                    (1. - rand_1 + rand_1 * (1. - (r / distance_to_center).powi(2)).sqrt()).acos();
                let phi = 2. * PI * rand_2;

                let direction_in_uvw = math::UnitVector::new_unchecked(math::Vector::new(
                    theta.sin() * phi.cos(),
                    theta.sin() * phi.sin(),
                    theta.cos(),
                ));
                let jacobian = na::Matrix3::from_columns(&[
                    u_hat.into_inner(),
                    v_hat.into_inner(),
                    w_hat.into_inner(),
                ]);
                let a_hat =
                    math::UnitVector::new_unchecked(jacobian * direction_in_uvw.into_inner());

                let ray_intersection = self
                    .toi_and_normal_with_ray(isometry, &math::Ray::new(*observer, a_hat))
                    .expect("Ray cast should be guaranteed to succeed");
                let point = observer + a_hat.into_inner() * ray_intersection.toi;
                let normal = ray_intersection.normal;
                let omega_prime = -a_hat;
                let area_pdf = omega_prime.dot(&normal)
                    / (2.
                        * PI
                        * (point - observer).norm().powi(2)
                        * (1. - (1. - (r / distance_to_center).powi(2)).sqrt()));
                (point, math::UnitVector::new_normalize(normal), area_pdf)
            }
            Shape::TriMesh {
                shape_container, ..
            } => {
                let (point, normal) = self.sample_point(isometry, rng);
                (point, normal, 1. / shape_container.surface_area)
            }
            Shape::Cuboid { shape_container } => {
                let (point, normal) = self.sample_point(isometry, rng);
                (point, normal, 1. / shape_container.surface_area)
            }
        }
    }
}

/// Struct to return from a ray cast.
pub struct RayIntersection {
    time_of_impact: Float,
    point: math::Point,
    normal: math::OrientedNormal,
}

/// Convenience alias for an axis-aligned bounding box.
type AxisAlignedBoundingBox = nc::bounding_volume::aabb::AABB<Float>;

/// A bounding-volume tree along with its objects.
struct BoundingVolumeTree {
    dbvt: nc::partitioning::DBVT<Float, usize, AxisAlignedBoundingBox>,
    objects: sync::Arc<Vec<Object>>,
}
/// Convenience alias for a leaf node of a bounding-volume tree.
type BvtLeaf = nc::partitioning::DBVTLeaf<Float, usize, AxisAlignedBoundingBox>;

impl BoundingVolumeTree {
    /// Make a new instance.
    pub fn new(objects: sync::Arc<Vec<Object>>) -> Self {
        let mut dbvt = nc::partitioning::DBVT::new();
        objects.iter().enumerate().for_each(|(index, object)| {
            dbvt.insert(BvtLeaf::new(object.axis_aligned_bounding_box(), index));
        });
        Self { dbvt, objects }
    }
    /// Find all the objects a ray intersects with, along with properties of the
    /// intersections.
    pub fn find_ray_intersections<'a>(&'a self, ray: &math::Ray) -> Vec<&'a Object> {
        use nc::partitioning::BVH;
        let mut buffer = Vec::<usize>::new();
        let ncollide_ray = ray.into();
        let mut coll = nc::query::visitors::RayInterferencesCollector::new(
            &ncollide_ray,
            MAX_TOI,
            &mut buffer,
        );
        self.dbvt.visit(&mut coll);
        buffer
            .iter()
            .cloned()
            .map(|index| self.objects.get(index).unwrap())
            .collect()
    }
}

#[derive(Debug)]
pub struct CollisionResult<'a> {
    pub point: math::Point,
    pub normal: math::OrientedNormal,
    pub object: &'a Object,
}

/// Enumeration of all supported reflection distribution functions.
#[derive(Debug, Clone)]
pub enum ScatteringParams {
    Matte(math::ScaledBSDF<math::MatteBSDF>),
    Glossy(math::CombinedBSDF<math::MatteBSDF, math::GlossyBSDF>),
    SpecularWithGlossy(
        math::CombinedBSDF<
            math::CombinedBSDF<math::SpecularBSDF, math::GlossyBSDF>,
            math::MatteBSDF,
        >,
    ),
    Specular(math::ScaledBSDF<math::SpecularBSDF>),
    SpecularWithMatte(math::CombinedBSDF<math::SpecularBSDF, math::MatteBSDF>),
    SpecularWithRefractive(math::CombinedBSDF<math::SpecularBSDF, math::RefractiveBSDF>),
}

impl ScatteringParams {
    /// Forward arguments to the appropriate BSDF's `eval` method.
    pub fn eval(
        &self,
        normal: &math::OrientedNormal,
        exitant: &math::UnitVector,
        incident: &math::UnitVector,
    ) -> math::DeltaFactor {
        use math::BSDF;
        match self {
            ScatteringParams::Matte(bsdf) => bsdf.eval(normal, exitant, incident),
            ScatteringParams::Glossy(bsdf) => bsdf.eval(normal, exitant, incident),
            ScatteringParams::SpecularWithGlossy(bsdf) => bsdf.eval(normal, exitant, incident),
            ScatteringParams::Specular(bsdf) => bsdf.eval(normal, exitant, incident),
            ScatteringParams::SpecularWithMatte(bsdf) => bsdf.eval(normal, exitant, incident),
            ScatteringParams::SpecularWithRefractive(bsdf) => bsdf.eval(normal, exitant, incident),
        }
    }

    /// Forward arguments to the appropriate BSDF's `importance_sample` method.
    pub fn importance_sample<RS: math::RandomSample>(
        &self,
        normal: &math::OrientedNormal,
        incident: &math::UnitVector,
        adjoint: bool,
        rng: &RS,
    ) -> (math::UnitVector, math::ImportanceSample) {
        use math::SamplingBSDF;
        match self {
            ScatteringParams::Matte(bsdf) => bsdf.importance_sample(normal, incident, adjoint, rng),
            ScatteringParams::Glossy(bsdf) => {
                bsdf.importance_sample(normal, incident, adjoint, rng)
            }
            ScatteringParams::SpecularWithGlossy(bsdf) => {
                bsdf.importance_sample(normal, incident, adjoint, rng)
            }
            ScatteringParams::Specular(bsdf) => {
                bsdf.importance_sample(normal, incident, adjoint, rng)
            }
            ScatteringParams::SpecularWithMatte(bsdf) => {
                bsdf.importance_sample(normal, incident, adjoint, rng)
            }
            ScatteringParams::SpecularWithRefractive(bsdf) => {
                bsdf.importance_sample(normal, incident, adjoint, rng)
            }
        }
    }
    pub fn eval_sampling_prob_density(
        &self,
        normal: &math::OrientedNormal,
        exitant: &math::UnitVector,
        incident: &math::UnitVector,
    ) -> math::DeltaFactor {
        use math::SamplingBSDF;
        match self {
            ScatteringParams::Matte(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
            ScatteringParams::Glossy(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
            ScatteringParams::SpecularWithGlossy(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
            ScatteringParams::Specular(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
            ScatteringParams::SpecularWithMatte(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
            ScatteringParams::SpecularWithRefractive(bsdf) => {
                bsdf.eval_sampling_prob_density(normal, exitant, incident)
            }
        }
    }

    pub fn matte(matte_frac: Float) -> Self {
        ScatteringParams::Matte(math::ScaledBSDF::<_>::new(math::MatteBSDF {}, matte_frac))
    }
    pub fn glossy(glossy_frac: Float, matte_frac: Float, glossy_fwhm_angle_rad: Float) -> Self {
        use math::Combinable;
        ScatteringParams::Glossy(math::MatteBSDF {}.combine_with(
            matte_frac,
            math::GlossyBSDF::new(glossy_fwhm_angle_rad),
            glossy_frac,
        ))
    }
    pub fn specular_with_glossy(
        spec_frac: Float,
        glossy_frac: Float,
        matte_frac: Float,
        glossy_fwhm_angle_rad: Float,
    ) -> Self {
        use math::Combinable;
        ScatteringParams::SpecularWithGlossy(
            math::SpecularBSDF {}
                .combine_with(
                    spec_frac,
                    math::GlossyBSDF::new(glossy_fwhm_angle_rad),
                    glossy_frac,
                )
                .combine_with(1., math::MatteBSDF {}, matte_frac),
        )
    }
    pub fn specular_with_matte(spec_frac: Float, matte_frac: Float) -> Self {
        use math::Combinable;
        ScatteringParams::SpecularWithMatte(math::SpecularBSDF {}.combine_with(
            spec_frac,
            math::MatteBSDF {},
            matte_frac,
        ))
    }
    pub fn specular(refl_frac: Float) -> Self {
        ScatteringParams::Specular(math::ScaledBSDF::<_>::new(math::SpecularBSDF {}, refl_frac))
    }
    pub fn specular_with_refractive(
        spec_frac: Float,
        refr_frac: Float,
        index_of_refraction: Float,
    ) -> Self {
        use math::Combinable;
        ScatteringParams::SpecularWithRefractive(math::SpecularBSDF::default().combine_with(
            spec_frac,
            math::RefractiveBSDF::new(index_of_refraction),
            refr_frac,
        ))
    }
}

/// The optical properties of an object's surface.
#[derive(Debug, Clone)]
pub enum SurfaceParams {
    /// Parameters controlling surface scattering.
    Scattering(ScatteringParams),
    /// The angular profile of emission, along with the amount of light that
    /// will be produced.
    Emitting(EmitProfile),
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;
    use nalgebra as na;
    use pretty_assertions::assert_eq;

    use super::*;
    use crate::{math, test_util as util};

    #[test]
    fn test_ray_cast_planes() {
        let scene = make_plane_scene();

        // From below.
        let result = scene
            .ray_cast(&util::make_ray([0., 0., -1.], [0., 0., 1.]))
            .unwrap();
        assert_relative_eq!(result.normal.dir, -math::Vector::z_axis());

        // From above.
        let result = scene
            .ray_cast(&util::make_ray([0., 0., 1.], [0., 0., -1.]))
            .unwrap();
        assert_relative_eq!(result.normal.dir, math::Vector::z_axis());
    }

    #[test]
    fn test_ray_cast_balls() {
        let scene = make_ball_scene();

        // Pointing up from the origin.
        let result = scene
            .ray_cast(&util::make_ray([0.; 3], [0., 0., 1.]))
            .unwrap();
        assert_eq!(
            result.object as *const Object,
            &scene.objects[0] as *const Object
        );
        assert_relative_eq!(result.normal.dir, -math::Vector::z_axis());
        assert!(result.normal.is_outward);
        assert_relative_eq!(result.point, math::Point::new(0., 0., 1.));

        // Pointing down from the origin.
        assert!(scene
            .ray_cast(&util::make_ray([0.; 3], [0., 0., -1.]))
            .is_none());

        // Pointing down from above first ball.
        let result = scene
            .ray_cast(&util::make_ray([0., 0., 4.], [0., 0., -1.]))
            .unwrap();
        util::assert_pointers_eq::<Object, _, _>(result.object, &scene.objects[0]);
        assert_relative_eq!(result.normal.dir, math::Vector::z_axis());
        assert!(result.normal.is_outward);
        assert_relative_eq!(result.point, math::Point::new(0., 0., 3.));

        // Pointing down from above both balls.
        let result = scene
            .ray_cast(&util::make_ray([0., 0., 40.], [0., 0., -1.]))
            .unwrap();
        util::assert_pointers_eq::<Object, _, _>(result.object, &scene.objects[1]);
        assert_relative_eq!(result.normal.dir, math::Vector::z_axis());
        assert!(result.normal.is_outward);
        assert_relative_eq!(result.point, math::Point::new(0., 0., 11.));
    }

    /// Make two balls placed along the z axis.
    fn make_ball_scene() -> Scene {
        let objects = vec![
            Object {
                shape: Shape::make_ball(1.),
                isometry: na::Isometry3::translation(0., 0., 2.),
                material: Material::with_surface_params(SurfaceParams::Emitting(
                    EmitProfile::identity(),
                )),
            },
            Object {
                shape: Shape::make_ball(1.),
                isometry: na::Isometry3::translation(0., 0., 10.),
                material: Material::with_surface_params(SurfaceParams::Scattering(
                    ScatteringParams::matte(0.),
                )),
            },
        ];
        Scene::from_objects(objects)
    }

    /// Make a single plane.
    fn make_plane_scene() -> Scene {
        let objects = vec![Object {
            shape: Shape::make_plane(&[
                math::Vector::new(100., 0., 0.),
                math::Vector::new(0., 100., 0.),
            ]),
            material: Material::with_surface_params(SurfaceParams::Scattering(
                ScatteringParams::matte(0.),
            )),
            isometry: na::Isometry3::identity(),
        }];
        Scene::from_objects(objects)
    }

    #[test]
    fn test_has_direct_line_of_sight() {
        {
            let scene = make_plane_scene();
            assert!(scene.has_direct_line_of_sight(
                &math::Point::new(1., 0., 1.),
                &math::Point::new(20., 0., 10.)
            ));
            assert!(scene.has_direct_line_of_sight(
                &math::Point::new(1., 0., 1.),
                &math::Point::new(20., 0., 0.)
            ));
            assert!(!scene.has_direct_line_of_sight(
                &math::Point::new(1., 0., 1.),
                &math::Point::new(20., 0., -1.)
            ));
        }
        {
            let scene = make_ball_scene();
            assert!(scene.has_direct_line_of_sight(
                &math::Point::new(0., 0., 4.),
                &math::Point::new(0., 0., 5.)
            ));
            assert!(!scene.has_direct_line_of_sight(
                &math::Point::new(0., 0., -1.),
                &math::Point::new(0., 0., 5.)
            ));
        }
    }

    #[test]
    fn test_surface_area() {
        // Ball.
        approx::assert_relative_eq!(
            4e-2 * PI,
            Shape::make_ball(0.1).surface_area(),
            epsilon = 1e-6
        );
        // Cuboid.
        approx::assert_relative_eq!(
            (0.1 * 2. + 0.1 * 3. + 2. * 3.) * 2.,
            Shape::make_cuboid(math::Vector::new(0.1, 2., 3.)).surface_area(),
            epsilon = 1e-6
        );
        // TriMesh.
        approx::assert_relative_eq!(
            4. * 2., // Just one side.
            Shape::make_tri_mesh(
                vec![
                    math::Point::new(0., 0., 0.),
                    math::Point::new(2., 0., 0.),
                    math::Point::new(0., 4., 0.),
                    math::Point::new(2., 4., 0.)
                ],
                vec![
                    na::Point3::<usize>::new(0, 1, 2),
                    na::Point3::<usize>::new(2, 1, 3)
                ]
            )
            .surface_area(),
            epsilon = 1e-6,
        )
    }

    #[test]
    fn test_sample_point_for_observer_ball() {
        let ball_displacement = math::Vector::new(1., 1., 2.);
        let isometry =
            math::Isometry::new(ball_displacement, PI / 2. * math::Vector::new(0., 0., 1.));
        let rng = math::DeterministicRandomEngine::default();
        let num_points = 10000usize;

        let ball = Shape::make_ball(0.7);

        let observer = math::Point::new(-100., 0., 0.);
        let center = isometry.transform_point(&math::Point::new(0., 0., 0.));
        let observer_to_center = math::UnitVector::new_normalize(center - observer);
        let mut displacement_sum = math::Vector::zeros();
        let mut area_sum = 0.;
        for _ in 0..num_points {
            let (point, _, pdf) = ball.sample_point_for_observer(&observer, &isometry, &rng);
            approx::assert_relative_eq!((point - center).norm(), 0.7, epsilon = 1e-5);
            displacement_sum += math::UnitVector::new_normalize(point - center).into_inner();
            area_sum += 1. / pdf;
        }
        let area_integral = area_sum / num_points as Float;
        approx::assert_relative_eq!(
            -math::UnitVector::new_normalize(displacement_sum).dot(&observer_to_center),
            1.,
            epsilon = 1e-4
        );
        approx::assert_relative_eq!(area_integral, 2. * PI * (0.7f64).powi(2), epsilon = 5e-2);
    }

    #[test]
    fn test_sample_point_ball_tri_mesh() {
        let displacement = math::Vector::new(1., 1., 2.);
        let isometry = math::Isometry::new(displacement, PI / 2. * math::Vector::new(0., 0., 1.));
        let rng = math::DeterministicRandomEngine::default();
        let num_points = 10000usize;

        // Ball.
        {
            let ball = Shape::make_ball(0.7);
            let mean_point = (0..num_points)
                .map(|_| {
                    let (point, _) = ball.sample_point(&isometry, &rng);
                    math::Vector::new(point[0], point[1], point[2])
                })
                .sum::<math::Vector>()
                / num_points as Float;
            approx::assert_relative_eq!(mean_point, displacement, epsilon = 1e-2);
            let mean_normal = (0..num_points)
                .map(|_| ball.sample_point(&isometry, &rng).1.into_inner())
                .sum::<math::Vector>()
                / num_points as Float;
            approx::assert_relative_eq!(mean_normal, math::Vector::new(0., 0., 0.), epsilon = 1e-2);
        }
        // TriMesh.
        {
            // Square formed by three triangles.
            //
            // 4  3  2
            // *--*--*
            // |/   \|
            // *-----*
            // 0     1
            let points = vec![
                math::Point::new(0., 0., 1.),
                math::Point::new(1., 0., 1.),
                math::Point::new(1., 1., 1.),
                math::Point::new(0.5, 1., 1.),
                math::Point::new(0., 1., 1.),
            ];
            let index_triplets = vec![
                na::Point3::<usize>::new(0, 1, 3),
                na::Point3::<usize>::new(1, 2, 3),
                na::Point3::<usize>::new(0, 3, 4),
            ];
            let tri_mesh = Shape::make_tri_mesh(points, index_triplets);

            let mean_point = (0..num_points)
                .map(|_| {
                    let (point, _) = tri_mesh.sample_point(&isometry, &rng);
                    math::Vector::new(point[0], point[1], point[2])
                })
                .sum::<math::Vector>()
                / num_points as Float;
            approx::assert_relative_eq!(
                mean_point,
                displacement + math::Vector::new(-0.5, 0.5, 1.),
                epsilon = 1e-2
            );
            for _ in 0..num_points {
                let (_, normal) = tri_mesh.sample_point(&isometry, &rng);
                assert!(normal == math::Vector::z_axis());
            }
        }
    }

    #[test]
    fn test_sample_total_luminosity() {
        let scene = make_emitting_balls_scene();
        let rng = math::DeterministicRandomEngine::default();

        let mut sampled_area_integral = 0.;
        for _ in 0..10000 {
            let emit_sample = scene.sample_emitting_point(&rng);
            sampled_area_integral +=
                emit_sample.emit_profile.integral() / emit_sample.area_prob_dens;
        }
        assert_relative_eq!(
            sampled_area_integral / 10000.,
            scene.objects[0].luminosity() + scene.objects[1].luminosity(),
            epsilon = 0.1
        );
    }

    #[test]
    fn test_sample_cross_sectional_area() {
        let scene = make_emitting_balls_scene();
        let rng = math::DeterministicRandomEngine::default();
        let observer_point = math::Point::new(100., 0., 0.);

        let mut sampled_cross_sec_area_integral = 0.;
        for _ in 0..10000 {
            let emit_sample = scene.sample_emitting_point(&rng);
            if scene.has_direct_line_of_sight(&observer_point, &emit_sample.point) {
                let displacement = observer_point - emit_sample.point;
                sampled_cross_sec_area_integral += emit_sample
                    .normal
                    .dot(&(math::UnitVector::new_normalize(displacement)))
                    / displacement.norm().powi(2)
                    / emit_sample.area_prob_dens;
            }
        }
        assert_relative_eq!(
            sampled_cross_sec_area_integral / 10000.,
            PI * (1f64.powi(2) / (100f64.powi(2) + 2f64.powi(2))
                + 2.5f64.powi(2) / (100f64.powi(2) + 10f64.powi(2))),
            max_relative = 0.01
        );
    }

    #[test]
    fn test_normal_inside_sphere() {
        let ball = Shape::make_ball(0.7);
        let ray_intersection = ball
            .toi_and_normal_with_ray(
                &math::Isometry::identity(),
                &math::Ray::new(math::Point::new(0., 0., 0.), math::Vector::x_axis()),
            )
            .unwrap();
        approx::assert_relative_eq!(
            ray_intersection.normal.dot(&math::Vector::x_axis()),
            -1.,
            epsilon = 1e-5
        );
    }

    fn make_emitting_balls_scene() -> Scene {
        let ball_1 = Object {
            shape: Shape::make_ball(1.),
            isometry: na::Isometry3::translation(0., 0., 2.),
            material: Material::with_surface_params(SurfaceParams::Emitting(
                EmitProfile::identity(),
            )),
        };
        let ball_2 = Object {
            shape: Shape::make_ball(2.5),
            isometry: na::Isometry3::translation(0., 0., 10.),
            material: Material::with_surface_params(SurfaceParams::Emitting(
                EmitProfile::isotropic(1.5),
            )),
        };
        Scene::from_objects(vec![ball_1.clone(), ball_2.clone()])
    }
}
