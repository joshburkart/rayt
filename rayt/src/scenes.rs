/// Enumeration of all available scenes for rendering.
use nalgebra as na;
use ndarray as nd;
use num_traits::Pow;

use raytlib::{
    self as rl,
    math::{self, x_hat, y_hat, z_hat},
    Float,
};

type RegistryMap = std::collections::HashMap<&'static str, fn(Float) -> rl::Renderable>;

pub fn make_renderable(name: &str, transition_param: Float) -> rl::Renderable {
    SCENE_REGISTRY.get(name).unwrap()(transition_param)
}

pub fn get_renderable_names() -> impl Iterator<Item = &'static str> {
    SCENE_REGISTRY.keys().cloned()
}

lazy_static::lazy_static! {
    /// We place all available scenes into a string-keyed registry, so that they can be set with
    /// command-line arguments.
    static ref SCENE_REGISTRY: RegistryMap = {
        let mut registry = RegistryMap::new();

        registry.insert("breakfast", breakfast::make_renderable);
        registry.insert("breakfast_b", breakfast::make_renderable_b);

        registry.insert("mirror_balls", mirror_balls::make_renderable);
        registry.insert("mirror_balls_b", mirror_balls::make_renderable_b);

        registry.insert("moonlit", moonlit::make_renderable);
        registry.insert("moonlit_b", moonlit::make_renderable_b);

        registry.insert("wicker", wicker::make_renderable);
        registry.insert("wicker_b", wicker::make_renderable_b);

        registry.insert("paperweights", paperweights::make_renderable);
        registry.insert("paperweights_b", paperweights::make_renderable_b);

        registry.insert("star", star::make_renderable);

        registry
    };
}

mod star {
    use super::*;

    const SMALL_STELLATION_DIST: Float = 1.;
    const LARGE_STELLATION_DIST: Float = 3.5;

    pub fn make_renderable(_transition_param: Float) -> rl::Renderable {
        let points = make_icosi_points(1.);
        let small_stellation_shape =
            make_stellation(&points, &make_triangles(&points), SMALL_STELLATION_DIST);
        let large_stellation_shape =
            make_stellation(&points, &make_pentagons(&points), LARGE_STELLATION_DIST);
        let small_stellation_obj = rl::Object {
            shape: small_stellation_shape,
            isometry: rl::Isometry::identity(),
            material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                rl::EmitProfile::new([(0, 1. / 2.), (2, 1. / 2.)]),
            )),
        };
        let large_stellation_obj = rl::Object {
            shape: large_stellation_shape,
            isometry: rl::Isometry::identity(),
            material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                rl::ScatteringParams::glossy(0.4, 0.2, (15f64).to_radians()),
            )),
        };
        let light_1 = rl::Object {
            shape: rl::Shape::make_ball(2.),
            isometry: rl::Isometry::translation(9., -20., 0.),
            material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                rl::EmitProfile::isotropic(100.),
            )),
        };
        let light_2 = rl::Object {
            shape: rl::Shape::make_ball(2.),
            isometry: rl::Isometry::translation(7., 20., 0.),
            material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                rl::EmitProfile::isotropic(50.),
            )),
        };
        let scene = rl::Scene::from_objects(vec![
            small_stellation_obj,
            large_stellation_obj,
            light_1,
            light_2,
            make_infinity_sphere(),
        ])
        .with_exposure(2.);
        rl::Renderable {
            scene,
            frame: rl::FrameBuilder {
                observer: math::Point::new(10.7, 1.4, 2.),
                vertical: rl::Vector::z_axis().into_inner(),
                center: math::Point::new(0., 0., 0.),
                aspect_ratio: 1.,
                height: 10.5,
            }
            .build(),
        }
    }

    pub fn make_icosi_points(scale: f64) -> Vec<rl::Point> {
        let phi: rl::Float = scale * (1. + (5f64).sqrt()) / 2.;
        vec![
            rl::Point::new(0., 0., phi),
            rl::Point::new(0., 0., -phi),
            rl::Point::new(0., phi, 0.),
            rl::Point::new(0., -phi, 0.),
            rl::Point::new(phi, 0., 0.),
            rl::Point::new(-phi, 0., 0.),
            rl::Point::new(1. / 2., phi / 2., phi.powi(2) / 2.),
            rl::Point::new(-1. / 2., phi / 2., phi.powi(2) / 2.),
            rl::Point::new(1. / 2., -phi / 2., phi.powi(2) / 2.),
            rl::Point::new(-1. / 2., -phi / 2., phi.powi(2) / 2.),
            rl::Point::new(1. / 2., phi / 2., -phi.powi(2) / 2.),
            rl::Point::new(-1. / 2., phi / 2., -phi.powi(2) / 2.),
            rl::Point::new(1. / 2., -phi / 2., -phi.powi(2) / 2.),
            rl::Point::new(-1. / 2., -phi / 2., -phi.powi(2) / 2.),
            rl::Point::new(phi / 2., phi.powi(2) / 2., 1. / 2.),
            rl::Point::new(-phi / 2., phi.powi(2) / 2., 1. / 2.),
            rl::Point::new(phi / 2., -phi.powi(2) / 2., 1. / 2.),
            rl::Point::new(-phi / 2., -phi.powi(2) / 2., 1. / 2.),
            rl::Point::new(phi / 2., phi.powi(2) / 2., -1. / 2.),
            rl::Point::new(-phi / 2., phi.powi(2) / 2., -1. / 2.),
            rl::Point::new(phi / 2., -phi.powi(2) / 2., -1. / 2.),
            rl::Point::new(-phi / 2., -phi.powi(2) / 2., -1. / 2.),
            rl::Point::new(phi.powi(2) / 2., 1. / 2., phi / 2.),
            rl::Point::new(-phi.powi(2) / 2., 1. / 2., phi / 2.),
            rl::Point::new(phi.powi(2) / 2., -1. / 2., phi / 2.),
            rl::Point::new(-phi.powi(2) / 2., -1. / 2., phi / 2.),
            rl::Point::new(phi.powi(2) / 2., 1. / 2., -phi / 2.),
            rl::Point::new(-phi.powi(2) / 2., 1. / 2., -phi / 2.),
            rl::Point::new(phi.powi(2) / 2., -1. / 2., -phi / 2.),
            rl::Point::new(-phi.powi(2) / 2., -1. / 2., -phi / 2.),
        ]
    }

    fn make_stellation<const N: usize>(
        points: &[rl::Point],
        polygons: &[Polygon<N>],
        stellation_dist: Float,
    ) -> rl::Shape {
        let mut tri_mesh_points = Vec::from(points);
        let mut tri_mesh_index_triplets = Vec::new();

        for polyhedron in polygons {
            let centroid = polyhedron.compute_centroid(points);
            let normal = polyhedron.compute_normal(points, &centroid);
            tri_mesh_points.push(centroid + stellation_dist * normal.outward_dir().into_inner());
            let tip_index = tri_mesh_points.len() - 1;

            for i in 0..N {
                let index = polyhedron.indices[i];
                let next_index = polyhedron.indices[(i + 1) % N];
                let next_point = if normal.is_outward {
                    na::Point3::<usize>::new(index, next_index, tip_index)
                } else {
                    na::Point3::<usize>::new(next_index, index, tip_index)
                };
                tri_mesh_index_triplets.push(next_point);
            }
        }

        rl::Shape::make_tri_mesh(tri_mesh_points, tri_mesh_index_triplets)
    }

    fn make_triangles(points: &[rl::Point]) -> Vec<Polygon<3>> {
        let mut triangles = Vec::<Polygon<3>>::new();

        for index in 0..points.len() {
            let four_closest_indices = find_four_closest(points, index);
            for close_index in four_closest_indices {
                let distance = compute_distance(points, index, close_index);
                let next_four_closest_indices = find_four_closest(points, close_index);
                for next_close_index in next_four_closest_indices {
                    if index == next_close_index {
                        continue;
                    }
                    if triangles.iter().any(|triangle| {
                        triangle.has(index)
                            && triangle.has(close_index)
                            && triangle.has(next_close_index)
                    }) {
                        continue;
                    }
                    let next_distance = compute_distance(points, index, next_close_index);
                    if (next_distance - distance).abs() > 1e-5 {
                        continue;
                    }
                    triangles.push(Polygon::<3>::new([index, close_index, next_close_index]));
                    break;
                }
            }
        }
        triangles
    }
    fn make_pentagons(points: &[rl::Point]) -> Vec<Polygon<5>> {
        let mut pentagons = Vec::<Polygon<5>>::new();

        for index in 0..points.len() {
            let four_closest_indices = find_four_closest(points, index);
            for close_index in four_closest_indices {
                if pentagons
                    .iter()
                    .any(|pentagon| pentagon.has(index) && pentagon.has(close_index))
                {
                    continue;
                }
                let mut prev_displacement_unit =
                    rl::UnitVector::new_normalize(points[close_index] - points[index]);
                let mut pentagon_indices = vec![index, close_index];
                for i in 1..4 {
                    let next_four_closest_indices = find_four_closest(points, pentagon_indices[i]);
                    for next_close_index in next_four_closest_indices {
                        if pentagon_indices[i - 1] == next_close_index {
                            continue;
                        }
                        let next_displacement_unit = rl::UnitVector::new_normalize(
                            points[next_close_index] - points[pentagon_indices[i]],
                        );
                        let angle = next_displacement_unit.dot(&prev_displacement_unit).acos();
                        if (angle - 2. * std::f64::consts::PI / 5.).abs() > 1e-5 {
                            continue;
                        }
                        pentagon_indices.push(next_close_index);
                        prev_displacement_unit = next_displacement_unit;
                        break;
                    }
                    assert_eq!(pentagon_indices.len(), i + 2);
                }
                pentagons.push(Polygon::<5>::new(pentagon_indices.try_into().unwrap()))
            }
        }
        pentagons
    }
    fn find_four_closest(points: &[rl::Point], index: usize) -> [usize; 4] {
        let mut dist_and_index_pairs = Vec::new();
        for other_index in 0..points.len() {
            if other_index == index {
                continue;
            }
            dist_and_index_pairs.push((compute_distance(points, index, other_index), other_index));
        }
        dist_and_index_pairs.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
        [
            dist_and_index_pairs[0].1,
            dist_and_index_pairs[1].1,
            dist_and_index_pairs[2].1,
            dist_and_index_pairs[3].1,
        ]
    }
    fn compute_distance(points: &[rl::Point], index_1: usize, index_2: usize) -> rl::Float {
        (points[index_1] - points[index_2]).norm()
    }

    #[derive(Debug)]
    struct Polygon<const N: usize> {
        indices: [usize; N],
    }
    impl<const N: usize> Polygon<N> {
        pub fn new(indices: [usize; N]) -> Self {
            Self { indices }
        }
        pub fn has(&self, index: usize) -> bool {
            self.indices.iter().any(|i| *i == index)
        }
        pub fn compute_centroid(&self, points: &[rl::Point]) -> rl::Point {
            rl::Point {
                coords: self
                    .indices
                    .iter()
                    .map(|index| points[*index].coords)
                    .sum::<rl::Vector>()
                    / (N as Float),
            }
        }
        pub fn compute_normal(
            &self,
            points: &[rl::Point],
            centroid: &rl::Point,
        ) -> rl::OrientedNormal {
            let normal = (points[self.indices[1]] - points[self.indices[0]])
                .cross(&(points[self.indices[2]] - points[self.indices[0]]));
            rl::OrientedNormal {
                dir: rl::UnitVector::new_normalize(normal),
                is_outward: normal.dot(&centroid.coords) > 0.,
            }
        }
    }
}

/// "Paperweights" scene/renderables.
mod paperweights {
    use super::*;

    const LIGHT_RADIUS: Float = 0.5;
    const CUBE_WIDTH: Float = 1.;
    const CUBE_HALF_WIDTH: Float = CUBE_WIDTH / 2.;
    const ROOM_HALF_WIDTH: Float = 7.;
    const SEPARATION: Float = 2.;

    enum SceneChoice {
        MultipleSimple,
        FloatingPolyhedron,
    }

    pub fn make_renderable(transition_param: Float) -> rl::Renderable {
        make_renderable_impl(make_scene(
            SceneChoice::FloatingPolyhedron,
            transition_param,
        ))
    }
    pub fn make_renderable_b(transition_param: Float) -> rl::Renderable {
        make_renderable_impl(make_scene(SceneChoice::MultipleSimple, transition_param))
    }

    fn make_renderable_impl(scene: rl::Scene) -> rl::Renderable {
        rl::Renderable {
            scene: scene
                .with_exposure(0.35 * (ROOM_HALF_WIDTH / LIGHT_RADIUS).powi(2))
                .with_contrast_level(2),
            frame: rl::FrameBuilder {
                observer: math::Point::new(3.5, 3.5, 5.),
                vertical: rl::Vector::z_axis().into_inner(),
                center: math::Point::new(1., 0., 1.2 * CUBE_HALF_WIDTH),
                aspect_ratio: 1.,
                height: 4.,
            }
            .build(),
        }
    }

    fn make_scene(scene_choice: SceneChoice, transition_param: f64) -> rl::Scene {
        let mut objects = vec![
            // Room walls.
            rl::Object {
                shape: rl::Shape::make_cuboid(math::Vector::new(
                    ROOM_HALF_WIDTH,
                    ROOM_HALF_WIDTH,
                    ROOM_HALF_WIDTH,
                )),
                isometry: rl::Isometry::translation(0., 0., ROOM_HALF_WIDTH),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::matte(0.7),
                )),
            },
            make_infinity_sphere(),
        ];
        match scene_choice {
            SceneChoice::FloatingPolyhedron => {
                let polyhedron = {
                    let tri_mesh_nc =
                        ncollide3d::transformation::convex_hull(&star::make_icosi_points(0.5));
                    rl::Shape::make_tri_mesh(
                        tri_mesh_nc.coords,
                        tri_mesh_nc
                            .indices
                            .unwrap_unified()
                            .into_iter()
                            .map(|index_triplet| {
                                na::Point3::<_>::new(
                                    index_triplet[0] as usize,
                                    index_triplet[1] as usize,
                                    index_triplet[2] as usize,
                                )
                            })
                            .collect(),
                    )
                };
                objects.extend(
                    [
                        // Overhead room light.
                        rl::Object {
                            shape: rl::Shape::make_disc(5. * LIGHT_RADIUS, -rl::Vector::z_axis()),
                            material: rl::Material::with_surface_params(
                                rl::SurfaceParams::Emitting(rl::EmitProfile::single_term(
                                    40,
                                    15. * (1f64 / 5.).powi(2),
                                )),
                            ),
                            isometry: rl::Isometry::translation(
                                SEPARATION * CUBE_HALF_WIDTH,
                                0.,
                                2. * ROOM_HALF_WIDTH - 0.1,
                            ),
                        },
                        // Diamond polyhedron paperweight.
                        rl::Object {
                            shape: polyhedron,
                            material: rl::Material::with_surface_params(
                                rl::SurfaceParams::Scattering(
                                    rl::ScatteringParams::specular_with_refractive(
                                        trig_transition(0., 0.1, transition_param),
                                        trig_transition(1., 0.88, transition_param),
                                        trig_transition(1., 2.4, transition_param),
                                    ),
                                ),
                            ),
                            isometry: rl::Isometry::new(
                                trig_transition(
                                    rl::Vector::new(
                                        SEPARATION * CUBE_HALF_WIDTH,
                                        0.,
                                        CUBE_HALF_WIDTH,
                                    ),
                                    rl::Vector::new(
                                        SEPARATION * CUBE_HALF_WIDTH,
                                        0.,
                                        3. * CUBE_HALF_WIDTH,
                                    ),
                                    transition_param,
                                ),
                                rl::Vector::zeros(),
                            ),
                        },
                    ]
                    .into_iter(),
                );
            }
            SceneChoice::MultipleSimple => {
                let glass = rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::specular_with_refractive(0.1, 0.84, 1.5),
                ));
                let sapphire = rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::specular_with_refractive(0.1, 0.88, 1.77),
                ));
                let diamond = rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::specular_with_refractive(0.1, 0.88, 2.4),
                ));
                let scene_center_displ = rl::Vector::new(1., -0., 0.);
                let theta = trig_transition(0., rl::PI, transition_param);
                let spotlight_displ =
                    scene_center_displ + 5.5 * rl::Vector::new(theta.sin(), theta.cos(), 0.6);
                objects.extend(
                    [
                        // Overhead room light.
                        rl::Object {
                            shape: rl::Shape::make_disc(5. * LIGHT_RADIUS, -rl::Vector::z_axis()),
                            material: rl::Material::with_surface_params(
                                rl::SurfaceParams::Emitting(rl::EmitProfile::single_term(0, 0.1)),
                            ),
                            isometry: rl::Isometry::translation(
                                SEPARATION * CUBE_HALF_WIDTH,
                                0.,
                                2. * ROOM_HALF_WIDTH - 0.1,
                            ),
                        },
                        // Spotlight.
                        rl::Object {
                            shape: rl::Shape::make_disc(
                                LIGHT_RADIUS,
                                -rl::UnitVector::new_normalize(
                                    spotlight_displ - scene_center_displ,
                                ),
                            ),
                            material: rl::Material::with_surface_params(
                                rl::SurfaceParams::Emitting(rl::EmitProfile::single_term(4, 1.5)),
                            ),
                            isometry: rl::Isometry::new(spotlight_displ, rl::Vector::zeros()),
                        },
                        // Sapphire sphere paperweight.
                        rl::Object {
                            shape: rl::Shape::make_ball(CUBE_HALF_WIDTH * 1.3),
                            isometry: rl::Isometry::new(
                                rl::Vector::new(
                                    -SEPARATION * 1.3 * CUBE_HALF_WIDTH,
                                    -SEPARATION * 0.8 * CUBE_HALF_WIDTH,
                                    CUBE_HALF_WIDTH,
                                ),
                                rl::Vector::zeros(),
                            ),
                            material: sapphire,
                        },
                        // Glass cube paperweight.
                        rl::Object {
                            shape: rl::Shape::make_cuboid(math::Vector::new(
                                CUBE_HALF_WIDTH,
                                CUBE_HALF_WIDTH,
                                CUBE_HALF_WIDTH,
                            )),
                            isometry: rl::Isometry::new(
                                rl::Vector::new(
                                    SEPARATION * CUBE_HALF_WIDTH,
                                    -SEPARATION * CUBE_HALF_WIDTH,
                                    CUBE_HALF_WIDTH * (2f64).sqrt(),
                                ),
                                rl::UnitVector::new_normalize(rl::Vector::x_axis().into_inner())
                                    .into_inner()
                                    * (rl::PI / 4.),
                            ),
                            material: glass,
                        },
                        // Diamond cone paperweight.
                        rl::Object {
                            shape: rl::Shape::make_cone(
                                1.7 * CUBE_HALF_WIDTH,
                                rl::Vector::z_axis(),
                                2.8 * CUBE_HALF_WIDTH,
                                4,
                            ),
                            material: diamond,
                            isometry: rl::Isometry::new(
                                rl::Vector::new(
                                    SEPARATION * CUBE_HALF_WIDTH,
                                    SEPARATION * CUBE_HALF_WIDTH,
                                    0.01,
                                ),
                                rl::Vector::new(0., 0., 0.),
                            ),
                        },
                    ]
                    .into_iter(),
                );
            }
        };
        rl::Scene::from_objects(objects)
    }
}

/// "Wicker ball" scene/renderables.
mod wicker {
    use super::*;

    const LIGHT_RADIUS: Float = 0.05;
    const BALL_RADIUS: Float = 1.;

    const NUM_HOOPS: usize = 12;
    const NUM_HOOP_SEGMENTS: usize = 40;
    const HOOP_HALF_WIDTH: Float = BALL_RADIUS / 15.;

    const ROOM_HALF_EXTENT: Float = 7.;

    pub fn make_renderable(transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene(transition_param)
                .with_exposure(4. * (BALL_RADIUS / LIGHT_RADIUS).pow(2)),
            frame: rl::FrameBuilder {
                observer: rl::Point::new(-BALL_RADIUS * 4.5, 0., 0.),
                center: rl::Point::new(ROOM_HALF_EXTENT, 0., 0.),
                vertical: rl::Vector::new(0., 0., 1.),
                height: ROOM_HALF_EXTENT,
                aspect_ratio: 1.,
            }
            .build(),
        }
    }
    pub fn make_renderable_b(transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene(transition_param)
                .with_exposure(0.4 * (ROOM_HALF_EXTENT / LIGHT_RADIUS).pow(2)),
            frame: rl::FrameBuilder {
                observer: rl::Point::new(-BALL_RADIUS * 4.5, 0., 0.),
                center: rl::Point::new(-ROOM_HALF_EXTENT, 0., 0.),
                vertical: rl::Vector::new(0., 0., 1.),
                height: 3. * ROOM_HALF_EXTENT,
                aspect_ratio: 1.,
            }
            .build(),
        }
    }

    fn make_scene(transition_param: Float) -> rl::Scene {
        let rng = rl::DeterministicRandomEngine::default();
        let ball_isometry = na::Isometry3::translation(ROOM_HALF_EXTENT - BALL_RADIUS, 0., 0.);

        let objects = vec![
            // Wicker ball.
            make_ball(&rng, ball_isometry, transition_param),
            // Light.
            rl::Object {
                shape: rl::Shape::make_ball(LIGHT_RADIUS),
                isometry: ball_isometry,
                material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                    rl::EmitProfile::new([(0, 1. / 2.), (2, 1. / 2.)]),
                )),
            },
            // Walls.
            rl::Object {
                shape: rl::Shape::make_cuboid(rl::Vector::new(
                    ROOM_HALF_EXTENT,
                    ROOM_HALF_EXTENT,
                    ROOM_HALF_EXTENT,
                )),
                isometry: na::Isometry3::identity(),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::matte(0.7),
                )),
            },
            make_infinity_sphere(),
        ];
        rl::Scene::from_objects(objects)
    }

    /// Make the set of hoops that comprise the wicker ball.
    fn make_ball(
        rng: &impl rl::RandomSample,
        isometry: na::Isometry3<Float>,
        transition_param: Float,
    ) -> rl::Object {
        let origin = rl::Point::origin();
        let make_random_point = || {
            let mut displacement = rl::Vector::new(
                rng.random_sample(&*rl::NORMAL_RNG_DIST),
                rng.random_sample(&*rl::NORMAL_RNG_DIST),
                rng.random_sample(&*rl::NORMAL_RNG_DIST),
            );
            displacement *= BALL_RADIUS / displacement.norm();
            displacement *= 1. + 0.1 * rng.random_float();
            origin + displacement
        };

        let mut tri_meshes = Vec::<_>::with_capacity(NUM_HOOPS);
        for _ in 0..NUM_HOOPS {
            let point_1 = make_random_point();
            let angular_speed = 2. * rl::PI * if rng.random_float() > 0.5 { 1. } else { 0.5 };
            let point_2 = rl::Isometry::rotation(
                angular_speed
                    * transition_param
                    * rl::UnitVector::new_normalize(point_1 - rl::Point::origin()).into_inner(),
            )
            .transform_point(&make_random_point());
            tri_meshes.push(make_hoop(&origin, point_1 * 0.99, point_2 * 0.99, false));
            tri_meshes.push(make_hoop(&origin, point_1, point_2, true));
        }
        rl::Object {
            shape: rl::Shape::combine_tri_meshes(tri_meshes),
            material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                rl::ScatteringParams::matte(0.7),
            )),
            isometry,
        }
    }

    /// Make a triangle mesh for a circular hoop that passes through two points.
    fn make_hoop(
        origin: &rl::Point,
        point_1: rl::Point,
        point_2: rl::Point,
        normal_outward: bool,
    ) -> (Vec<rl::Point>, Vec<na::Point3<usize>>) {
        // Construct an orthonormal basis, where the first two basis vectors are in the
        // plane of the hoop, and the third is orthogonal.
        let (basis_vec_1, radius) = {
            let mut basis_vec_1 = point_1 - origin;
            let radius = basis_vec_1.normalize_mut();
            (basis_vec_1, radius)
        };
        let basis_vec_2 = {
            let mut basis_vec_2 = point_2 - origin;
            basis_vec_2 -= basis_vec_1 * basis_vec_2.dot(&basis_vec_1) / basis_vec_2.norm();
            basis_vec_2
        };
        let basis_vec_3 =
            rl::UnitVector::new_normalize(basis_vec_1.cross(&basis_vec_2)).into_inner();

        let mut points = Vec::<rl::Point>::with_capacity(2 * NUM_HOOP_SEGMENTS);
        let mut index_triplets = Vec::<na::Point3<usize>>::with_capacity(2 * NUM_HOOP_SEGMENTS);
        for i in 0..NUM_HOOP_SEGMENTS {
            let angle = 2. * rl::PI * (i as Float) / ((NUM_HOOP_SEGMENTS - 1) as Float);
            let base_point =
                origin + radius * (angle.cos() * basis_vec_1 + angle.sin() * basis_vec_2);

            points.push(base_point + HOOP_HALF_WIDTH * basis_vec_3);
            points.push(base_point - HOOP_HALF_WIDTH * basis_vec_3);

            if normal_outward {
                index_triplets.push(na::Point3::<usize>::new(
                    2 * i,
                    2 * i + 1,
                    (2 * i + 2) % (2 * NUM_HOOP_SEGMENTS),
                ));
                index_triplets.push(na::Point3::<usize>::new(
                    2 * i + 1,
                    (2 * i + 2) % (2 * NUM_HOOP_SEGMENTS),
                    (2 * i + 3) % (2 * NUM_HOOP_SEGMENTS),
                ));
            } else {
                index_triplets.push(na::Point3::<usize>::new(
                    2 * i + 1,
                    2 * i,
                    (2 * i + 2) % (2 * NUM_HOOP_SEGMENTS),
                ));
                index_triplets.push(na::Point3::<usize>::new(
                    (2 * i + 2) % (2 * NUM_HOOP_SEGMENTS),
                    2 * i + 1,
                    (2 * i + 3) % (2 * NUM_HOOP_SEGMENTS),
                ));
            }
        }
        (points, index_triplets)
    }
}

/// "Moonlit sea" scene/renderables.
mod moonlit {
    use super::*;

    type Array1 = nd::Array1<Float>;
    type Array2 = nd::Array2<Float>;

    const X_MAX: Float = 100. / 2.;
    const Y_MAX: Float = 100. / 2.;
    const X_POINTS: usize = 1000;
    const Y_POINTS: usize = X_POINTS;

    const WAVELENGTH: Float = 1.5;

    const MOON_INTENSITY: Float = 1.;
    const MOON_RADIUS: Float = 1.;

    const Z_SCALE: Float = WAVELENGTH / 20.;

    pub fn make_renderable(_transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene().with_exposure(30.),
            frame: rl::FrameBuilder {
                observer: math::Point::new(0., 0., 7.),
                vertical: rl::Vector::z_axis().into_inner(),
                center: math::Point::new(10., 10., 2.),
                aspect_ratio: 1.,
                height: 5.,
            }
            .build(),
        }
    }

    pub fn make_renderable_b(_transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene().with_exposure(30.),
            frame: rl::FrameBuilder {
                observer: math::Point::new(20., 20., 20.),
                vertical: rl::Vector::x_axis().into_inner(),
                center: math::Point::new(20., 20., 0.),
                aspect_ratio: 1.,
                height: 20.,
            }
            .build(),
        }
    }

    fn make_scene() -> rl::Scene {
        let objects: Vec<rl::Object> = vec![
            rl::Object {
                shape: make_sea_height_map().to_tri_mesh(),
                isometry: na::Isometry3::identity(),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::specular_with_glossy(0., 0.2, 0.5, 6f64.to_radians()),
                )),
            },
            rl::Object {
                shape: rl::Shape::make_ball(MOON_RADIUS),
                isometry: na::Isometry3::translation(20., 20., 6.),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                    rl::EmitProfile::new([(0, MOON_INTENSITY / 2.), (2, MOON_INTENSITY / 2.)]),
                )),
            },
            make_infinity_sphere(),
        ];

        rl::Scene::from_objects(objects)
    }

    fn make_sea_height_map() -> HeightMap {
        let x = Array1::linspace(0., X_MAX, X_POINTS);
        let y = Array1::linspace(0., Y_MAX, Y_POINTS);
        let mut z = Array2::zeros((X_POINTS, Y_POINTS));

        for (i, xi) in x.iter().enumerate() {
            for (j, yj) in y.iter().enumerate() {
                let arg_sq: Float = xi.pow(2) + yj.pow(2);
                z[(i, j)] = Z_SCALE * (arg_sq.sqrt() * 2. * rl::PI / WAVELENGTH).sin();
            }
        }

        HeightMap { x, y, z }
    }

    struct HeightMap {
        x: Array1,
        y: Array1,
        // x index is the first, y index is second.
        z: Array2,
    }

    impl HeightMap {
        pub fn to_tri_mesh(&self) -> rl::Shape {
            let mut points = Vec::<rl::Point>::with_capacity(X_POINTS * Y_POINTS);
            let mut tri_indices = Vec::<na::Point3<usize>>::with_capacity(2 * X_POINTS * Y_POINTS);

            for (i, xi) in self.x.iter().enumerate() {
                for (j, yj) in self.y.iter().enumerate() {
                    points.push(rl::Point::new(*xi, *yj, self.z[(i, j)]));
                }
            }

            let make_flat_index = |i: usize, j: usize| -> usize { i * Y_POINTS + j };

            for i in 0..self.x.len() - 1 {
                for j in 0..self.y.len() - 1 {
                    let lower_left = make_flat_index(i, j);
                    let lower_right = make_flat_index(i, j + 1);
                    let upper_left = make_flat_index(i + 1, j);
                    let upper_right = make_flat_index(i + 1, j + 1);

                    tri_indices.push(na::Point3::<usize>::new(
                        lower_left,
                        lower_right,
                        upper_left,
                    ));
                    tri_indices.push(na::Point3::<usize>::new(
                        lower_right,
                        upper_right,
                        upper_left,
                    ));
                }
            }

            rl::Shape::make_tri_mesh(points, tri_indices)
        }
    }
}

/// "Breakfast table" scene/renderables.
mod breakfast {
    use super::*;

    const TABLE_TOP_DEPTH: Float = 2. / 12.;
    const TABLE_WIDTH: Float = 2.5;
    const TABLE_HEIGHT: Float = 3.;

    const WINDOW_WIDTH: Float = 1.75;
    const WINDOW_SEP_HORIZ: Float = 0.4;
    const WINDOW_SEP_VERT: Float = 0.2;
    const NUM_WINDOW_ROWS: usize = 6;

    const TOP_PANEL_HEIGHT: Float = 0.4;
    const BOTTOM_PANEL_HEIGHT: Float = 3.;

    const ROOM_WIDTH: Float = 12.;
    const ROOM_DEPTH: Float = ROOM_WIDTH;
    const ROOM_HEIGHT: Float = 8.5;

    const HALF_WINDOW_SEP: Float = WINDOW_SEP_HORIZ / 2.;
    const HALF_ROOM_WIDTH: Float = ROOM_WIDTH / 2.;
    const HALF_ROOM_DEPTH: Float = ROOM_DEPTH / 2.;
    const HALF_ROOM_HEIGHT: Float = ROOM_HEIGHT / 2.;

    const GLOBE_RADIUS: Float = 0.65;

    const LIGHT_INTENSITY: Float = 1.;

    // Generate the main renderable.
    pub fn make_renderable(_transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene(),
            frame: rl::FrameBuilder {
                observer: math::Point::new(
                    -0.25001 * HALF_ROOM_DEPTH,
                    -0.25 * HALF_ROOM_DEPTH,
                    0.65 * ROOM_HEIGHT,
                ),
                vertical: rl::Vector::z_axis().into_inner(),
                center: math::Point::new(0.4, 0.4, 0.7 * HALF_ROOM_HEIGHT),
                aspect_ratio: 1.,
                height: 0.45 * ROOM_HEIGHT,
            }
            .build(),
        }
    }

    // Generate the "behind-the-scenes" renderable.
    pub fn make_renderable_b(_transition_param: Float) -> rl::Renderable {
        rl::Renderable {
            scene: make_scene(),
            frame: rl::FrameBuilder {
                observer: math::Point::new(
                    -0.85001 * HALF_ROOM_DEPTH,
                    -0.95 * HALF_ROOM_DEPTH,
                    0.95 * ROOM_HEIGHT,
                ),
                vertical: rl::Vector::z_axis().into_inner(),
                center: math::Point::new(0.4, 0.4, 0.7 * HALF_ROOM_HEIGHT),
                aspect_ratio: 1.,
                height: 0.95 * ROOM_HEIGHT,
            }
            .build(),
        }
    }

    fn make_scene() -> rl::Scene {
        let table_material = rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
            // rl::ScatteringParams::specular_with_matte(0.45, 0.4),
            rl::ScatteringParams::specular_with_glossy(0.05, 0.4, 0.4, (1f64).to_radians()),
        ));

        let mut objects = vec![
            // Rectangular table.
            rl::Object {
                shape: rl::Shape::make_cuboid(rl::Vector::new(
                    TABLE_WIDTH,
                    TABLE_WIDTH,
                    TABLE_TOP_DEPTH / 2.,
                )),
                isometry: na::Isometry3::new(math::Vector::new(0., 0., TABLE_HEIGHT), 0. * x_hat()),
                material: table_material.clone(),
            },
            // Left wall.
            RectBuilder {
                lower_left: math::Point::new(-HALF_ROOM_WIDTH, -HALF_ROOM_DEPTH, 0.),
                up_vec: z_hat() * ROOM_HEIGHT,
                right_vec: y_hat() * ROOM_DEPTH,
            }
            .build(),
            // Right wall.
            RectBuilder {
                lower_left: math::Point::new(HALF_ROOM_WIDTH, -HALF_ROOM_DEPTH, 0.),
                up_vec: z_hat() * ROOM_HEIGHT,
                right_vec: y_hat() * ROOM_DEPTH,
            }
            .build(),
            // Back wall.
            RectBuilder {
                lower_left: math::Point::new(-HALF_ROOM_WIDTH, -HALF_ROOM_DEPTH, 0.),
                up_vec: z_hat() * ROOM_HEIGHT,
                right_vec: x_hat() * ROOM_DEPTH,
            }
            .build(),
            // Front wall.
            RectBuilder {
                lower_left: math::Point::new(-HALF_ROOM_WIDTH, HALF_ROOM_DEPTH, 0.),
                up_vec: z_hat() * ROOM_HEIGHT,
                right_vec: x_hat() * ROOM_DEPTH,
            }
            .build(),
            // Floor.
            RectBuilder {
                lower_left: math::Point::new(-HALF_ROOM_WIDTH, -HALF_ROOM_DEPTH, 0.),
                up_vec: x_hat() * ROOM_WIDTH,
                right_vec: y_hat() * ROOM_DEPTH,
            }
            .build(),
            // Ceiling.
            RectBuilder {
                lower_left: math::Point::new(-HALF_ROOM_WIDTH, -HALF_ROOM_DEPTH, ROOM_HEIGHT),
                up_vec: x_hat() * ROOM_WIDTH,
                right_vec: y_hat() * ROOM_DEPTH,
            }
            .build(),
            // Light globe.
            rl::Object {
                shape: rl::Shape::make_ball(GLOBE_RADIUS),
                isometry: na::Isometry3::new(
                    math::Vector::new(0.15 * ROOM_WIDTH, 0.15 * ROOM_DEPTH, ROOM_HEIGHT),
                    0. * x_hat(),
                ),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                    rl::EmitProfile::isotropic(LIGHT_INTENSITY * 2. / 3.),
                )),
            },
            make_infinity_sphere(),
        ];
        let pane_height = (ROOM_HEIGHT - BOTTOM_PANEL_HEIGHT - TOP_PANEL_HEIGHT)
            / NUM_WINDOW_ROWS as Float
            - WINDOW_SEP_VERT;
        for i in 0..NUM_WINDOW_ROWS {
            for offset in [0., 1.] {
                objects.push(
                    RectBuilder {
                        lower_left: math::Point::new(
                            -HALF_WINDOW_SEP - WINDOW_WIDTH
                                + offset * (WINDOW_SEP_HORIZ + WINDOW_WIDTH),
                            HALF_ROOM_DEPTH * 0.99,
                            BOTTOM_PANEL_HEIGHT + (pane_height + WINDOW_SEP_VERT) * (i as Float),
                        ),
                        right_vec: x_hat() * WINDOW_WIDTH,
                        up_vec: z_hat() * pane_height,
                    }
                    .build_with_material(rl::Material::with_surface_params(
                        rl::SurfaceParams::Emitting(rl::EmitProfile::isotropic(
                            LIGHT_INTENSITY / 2.,
                        )),
                    )),
                );
            }
        }
        // Make window panes.
        let num_panes = 8;
        let x: Float = (ROOM_HEIGHT - BOTTOM_PANEL_HEIGHT - TOP_PANEL_HEIGHT
            + 2. * WINDOW_SEP_VERT)
            / ROOM_HEIGHT
            / (num_panes as Float + 1.);
        for i in 1..=num_panes {
            objects.push(
                RectBuilder {
                    lower_left: math::Point::new(
                        -HALF_ROOM_WIDTH,
                        HALF_ROOM_DEPTH,
                        BOTTOM_PANEL_HEIGHT + i as Float * x * ROOM_HEIGHT - WINDOW_SEP_VERT,
                        // Derivation:
                        //   rh - (tph + x * np * rh - wsv) = bph + x * 1 * rh - wsv
                        //   rh - bph - tph + 2wsv = x * (np + 1) rh
                        //   x = (rh - bph - tph + 2wsv) / rh / (np + 1)
                    ),
                    up_vec: z_hat() * WINDOW_SEP_VERT,
                    right_vec: x_hat() * ROOM_WIDTH,
                }
                .build(),
            );
        }

        rl::Scene::from_objects(objects).with_exposure(4.)
    }

    /// Helper to make an opaque rectangle.
    struct RectBuilder {
        lower_left: rl::Point,
        right_vec: rl::Vector,
        up_vec: rl::Vector,
    }

    impl RectBuilder {
        pub fn build(self) -> rl::Object {
            let material = rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                rl::ScatteringParams::glossy(0.35, 0.35, 30f64.to_radians()),
            ));
            self.build_with_material(material)
        }
        fn build_with_material(self, material: rl::Material) -> rl::Object {
            let lower_left = self.lower_left;
            let upper_left = lower_left + self.up_vec;
            let lower_right = lower_left + self.right_vec;
            let upper_right = lower_right + self.up_vec;

            // 3     2
            // UL---UR
            // |   / |
            // |  /  |
            // | /   |
            // LL---LR
            // 0     1
            rl::Object {
                shape: rl::Shape::make_tri_mesh(
                    vec![lower_left, lower_right, upper_right, upper_left],
                    vec![na::Point3::new(0usize, 1, 2), na::Point3::new(0, 2, 3)],
                ),
                isometry: na::Isometry3::identity(),
                material,
            }
        }
    }
}

/// "Balls in a mirror cube" scene/renderables.
mod mirror_balls {
    use super::*;

    const BOX_HALF_WIDTH: Float = 4.;
    const REFLECT_WEIGHT: Float = 0.85;
    const BALL_INTENSITY: Float = 1.;

    /// Rendering with the reflecting cube included.
    pub fn make_renderable(_transition_param: Float) -> rl::Renderable {
        make_renderable_impl(true)
    }

    /// Renderable without the reflecting cube.
    pub fn make_renderable_b(_transition_param: Float) -> rl::Renderable {
        make_renderable_impl(false)
    }

    fn make_renderable_impl(specular_cube: bool) -> rl::Renderable {
        let mut objects = vec![
            // Emitting balls.
            rl::Object {
                shape: rl::Shape::make_ball(0.5),
                isometry: na::Isometry3::translation(0., 2., 1.),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                    rl::EmitProfile::isotropic(BALL_INTENSITY),
                )),
            },
            rl::Object {
                shape: rl::Shape::make_ball(0.5),
                isometry: na::Isometry3::translation(2., 0., 0.),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Emitting(
                    rl::EmitProfile::new([(0, BALL_INTENSITY / 2.), (2, BALL_INTENSITY / 2.)]),
                )),
            },
            // Reflecting ball.
            rl::Object {
                shape: rl::Shape::make_ball(1.3),
                isometry: na::Isometry3::translation(2., 2., -1.),
                material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                    rl::ScatteringParams::specular(0.6),
                )),
            },
            make_infinity_sphere(),
        ];
        // Reflecting cube.
        let cube_material = if specular_cube {
            rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                rl::ScatteringParams::specular(REFLECT_WEIGHT),
            ))
        } else {
            rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
                // rl::ScatteringParams::glossy(0.7, 0.5, 500.),
                rl::ScatteringParams::matte(0.7),
            ))
        };
        objects.push(rl::Object {
            shape: rl::Shape::make_cuboid(BOX_HALF_WIDTH * rl::Vector::new(1., 1., 1.)),
            isometry: na::Isometry3::identity(),
            material: cube_material,
        });

        rl::Renderable {
            scene: rl::Scene::from_objects(objects).with_exposure(if specular_cube {
                1.
            } else {
                10.
            }),
            frame: rl::FrameBuilder {
                observer: math::Point::new(-3., -3., 0.),
                center: math::Point::new(0.75, 0.75, 0.),
                vertical: rl::math::z_hat(),
                aspect_ratio: 1.,
                height: 3.8,
            }
            .build(),
        }
    }
}

fn lin_transition<T>(x1: T, x2: T, transition_param: Float) -> T
where
    T: std::ops::Mul<Float, Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Add<Output = T>
        + Copy,
    Float: std::ops::Mul<T, Output = T>,
{
    x1 + transition_param * (x2 - x1)
}

fn trig_transition<T>(x1: T, x2: T, transition_param: Float) -> T
where
    T: std::ops::Mul<Float, Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Add<Output = T>
        + Copy,
{
    x1 + (x2 - x1) * ((-(transition_param * rl::PI).cos() + 1.) / 2.)
}

fn make_infinity_sphere() -> rl::Object {
    rl::Object {
        shape: rl::Shape::make_ball(1000.),
        isometry: na::Isometry3::identity(),
        material: rl::Material::with_surface_params(rl::SurfaceParams::Scattering(
            rl::ScatteringParams::matte(0.),
        )),
    }
}
