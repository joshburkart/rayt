//! Binary to render scenes.

mod scenes;

fn main() {
    let matches = clap::App::new("rayt")
        .arg(
            clap::Arg::with_name("scene")
                .possible_values(&scenes::get_renderable_names().collect::<Vec<_>>())
                .help("Which scene to render")
                .required(true)
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("transition_param")
                .long("transition_param")
                .help("Value of the animation transition parameter")
                .default_value("0")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("eye_trace_depth")
                .long("eye_trace_depth")
                .help("Maximum number of eye ray casts deep to render")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("light_trace_depth")
                .long("light_trace_depth")
                .help("Maximum number of light ray casts deep to render")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("num_samples")
                .long("num_samples")
                .help("The total number of samples to use for each pixel (for diffuse effects)")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("min_num_samples")
                .long("min_num_samples")
                .help("The minimum number of samples to use for each pixel (for diffuse effects)")
                .default_value("25")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("max_num_samples")
                .long("max_num_samples")
                .help("The maximum number of samples to use for each pixel (for diffuse effects)")
                .default_value("500")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("converge_abs_tol")
                .long("converge_abs_tol")
                .help(
                    "The absolute tolerance to use when assessing convergence (for diffuse \
                    effects)",
                )
                .default_value("0.02")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("height_pixels")
                .long("height_pixels")
                .help("Height of the rendered image in pixels")
                .default_value("800")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("output_path")
                .long("output_path")
                .help("Where to store the output image (if unspecified, output to `examples/`)")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("no_parallelize")
                .long("no_parallelize")
                .help("Whether not to parallelize rendering"),
        )
        .get_matches();
    let scene_name = matches.value_of("scene").unwrap();
    let transition_param = matches
        .value_of("transition_param")
        .unwrap()
        .parse::<f64>()
        .unwrap();
    let eye_trace_depth = matches
        .value_of("eye_trace_depth")
        .map(|depth| depth.parse::<usize>().unwrap());
    let light_trace_depth = matches
        .value_of("light_trace_depth")
        .map(|depth| depth.parse::<usize>().unwrap());
    let mut min_num_samples = matches
        .value_of("min_num_samples")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let mut max_num_samples = matches
        .value_of("max_num_samples")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    if let Some(num_samples) = matches.value_of("num_samples") {
        let num_samples = num_samples.parse().unwrap();
        min_num_samples = num_samples;
        max_num_samples = num_samples;
    }
    let converge_abs_tol = matches
        .value_of("converge_abs_tol")
        .unwrap()
        .parse::<f64>()
        .unwrap();
    let height_pixels = matches
        .value_of("height_pixels")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let output_path = matches
        .value_of("output_path")
        .map(|path| path.to_string())
        .unwrap_or_else(|| format!("examples/{}.png", scene_name));

    let ray_tracer = raytlib::DeterministicRayTracer::default()
        .with_eye_trace_depth(eye_trace_depth)
        .with_light_trace_depth(light_trace_depth)
        .with_min_num_samples(min_num_samples)
        .with_max_num_samples(max_num_samples)
        .with_converge_abs_tol(converge_abs_tol);
    let renderer = {
        let mut renderer = raytlib::Renderer::new(&ray_tracer);
        if matches.is_present("no_parallelize") {
            renderer = renderer.with_num_threads(1);
        }
        renderer
    };
    let renderable = scenes::make_renderable(scene_name, transition_param);

    let image = renderer.render_with_progress(renderable, height_pixels);
    std::fs::create_dir_all(
        AsRef::<std::path::Path>::as_ref(&output_path)
            .parent()
            .unwrap(),
    )
    .unwrap();
    image.save(&output_path).unwrap();
    println!("Output file to {output_path}");
}
