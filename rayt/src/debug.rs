//! Binary to debug with individual rays.

mod scenes;

fn main() {
    let matches = clap::App::new("rayt")
        .arg(
            clap::Arg::with_name("scene")
                .possible_values(&scenes::get_renderable_names().collect::<Vec<_>>())
                .required(true)
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("transition_param")
                .long("transition_param")
                .help("Value of the animation transition parameter")
                .default_value("0")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("eye_trace_depth")
                .long("eye_trace_depth")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("light_trace_depth")
                .long("light_trace_depth")
                .default_value("4")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("height_pixels")
                .long("height_pixels")
                .default_value("800")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("num_samples")
                .long("num_samples")
                .default_value("10")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("extra_info_sample_index")
                .long("extra_info_sample_index")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("x")
                .long("x")
                .required(true)
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("y")
                .long("y")
                .required(true)
                .takes_value(true),
        )
        .get_matches();
    let scene_name = matches.value_of("scene").unwrap();
    let transition_param = matches
        .value_of("transition_param")
        .unwrap()
        .parse::<f64>()
        .unwrap();
    let eye_trace_depth = matches
        .value_of("eye_trace_depth")
        .map(|depth| depth.parse::<usize>().unwrap());
    let light_trace_depth = matches
        .value_of("light_trace_depth")
        .map(|depth| depth.parse::<usize>().unwrap());
    let height_pixels = matches
        .value_of("height_pixels")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let num_samples = matches
        .value_of("num_samples")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let extra_info_sample_index = matches
        .value_of("extra_info_sample_index")
        .map(|x| x.parse::<usize>().unwrap());

    let x_index = matches.value_of("x").unwrap().parse::<usize>().unwrap();
    let y_index = matches.value_of("y").unwrap().parse::<usize>().unwrap();

    let raytlib::Renderable { scene, frame } =
        scenes::make_renderable(scene_name, transition_param);
    let ray = frame.image_coords_to_ray(frame.dimensions(height_pixels), (x_index, y_index));

    println!("Ray: {:?}", ray);

    let mut stats_stream = raytlib::StatsStream::default();
    let ray_tracer = raytlib::DeterministicRayTracer::default()
        .with_eye_trace_depth(eye_trace_depth)
        .with_light_trace_depth(light_trace_depth)
        .with_num_samples(1);
    for sample_index in 0..num_samples {
        let (eye_trace, light_trace) = ray_tracer.run_path_traces(&scene, &ray);
        let intensity_calculator = ray_tracer.eval_trace(&scene, &eye_trace, &light_trace);
        if Some(sample_index) == extra_info_sample_index {
            println!("\n\n{:#?}\n", intensity_calculator);
            println!("Eye:\n{:#?}\n", eye_trace);
            println!("Light:\n{:#?}\n\n", light_trace);
        }
        let intensity = intensity_calculator.calculate();
        stats_stream.append(intensity);
        println!("Sample {}: {}", sample_index, intensity);
    }

    println!(
        "\nMean\n  {:.4e}\nStandard dev.\n  {:.4e}\nStandard error\n  {:.4e}",
        stats_stream.mean(),
        stats_stream.standard_dev(),
        stats_stream.standard_error()
    );
    println!(
        "Samples required for standard error = mean / 10\n  {:.1}",
        (10. * stats_stream.standard_dev() / stats_stream.mean()).powf(2.)
    )
}
